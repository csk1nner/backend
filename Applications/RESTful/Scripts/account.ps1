﻿#$doesExist = Invoke-RestMethod -Uri "http://localhost:88/api/User/GetDoesEmailExist/test@test.com" -Method Get  -ContentType "application/json"
#echo  "Does email exist : $doesExist"

$createToken = Invoke-RestMethod -Uri "http://localhost:88/api/Account/Create" -Method Post -Body '{"email": "1212121test@test.com", "password": "TEST1234test!@!"}' -ContentType "application/json"
echo  "CreateToken : $createToken"
$token = $createToken[0].tokenString
echo  "Token : $token"
$retrieveUser = Invoke-RestMethod -Uri "http://localhost:88/api/User/Retrieve" -Headers @{ "Authorization"="Bearer $token" } -Method Post -Body '{"email": "1212121test@test.com", "password": "TEST1234test!@!"}' -ContentType "application/json"
echo  "User : $retrieveUser"
