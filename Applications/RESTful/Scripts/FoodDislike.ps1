﻿$loginToken = Invoke-RestMethod -Uri "http://localhost:88/api/Account/Login" -Method Post -Body '{"email": "test@test.com", "password": "TEST1234test!@!"}' -ContentType "application/json"
$token = $loginToken[0].token
echo  "token : $token"
$fooddislikes = Invoke-WebRequest -Uri "http://localhost:88/api/FoodDislike/Get/" -Headers @{ "Authorization"="Bearer $token" } -Method GET  -ContentType "application/json"
echo  "fooddislikes : $fooddislikes"

#$id = Invoke-RestMethod -Uri "http://localhost:88/api/FoodDislike/Create/" -Headers @{ "Authorization"="Bearer $token" } -Method Post -Body '{"foodid": "54076A9D-5E74-E111-BB4F-00155D0A0C06"}' -ContentType "application/json"
$id = Invoke-RestMethod -Uri "http://localhost:88/api/FoodDislike/Create/" -Headers @{ "Authorization"="Bearer $token" } -Method Post -Body '{"mealcomponentid": "B7962B43-D413-E111-8D19-00155D0A0205"}' -ContentType "application/json"
																															
$foodDislikeId = $id[0].id

echo  "fooddislikeId : $foodDislikeId"
$updateParams = @{foodid="7D6E5612-0201-E111-8F4E-00155D0A0205";fooddislikeid=$foodDislikeId}
echo $updateParams
$params =  ConvertTo-Json $updateParams
echo $params 
$update = Invoke-WebRequest -Uri "http://localhost:88/api/FoodDislike/Update/" -Headers @{ "Authorization"="Bearer $token" } -Method Patch -Body $params  -ContentType "application/json"
echo  "fooddislike update : $update"

$delete = Invoke-WebRequest -Uri "http://localhost:88/api/FoodDislike/Delete/$foodDislikeId" -Headers @{ "Authorization"="Bearer $token" } -Method Delete -ContentType "application/json"
echo  "fooddislike delete : $delete"
