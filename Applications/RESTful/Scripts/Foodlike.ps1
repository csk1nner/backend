﻿$loginToken = Invoke-RestMethod -Uri "http://localhost:88/api/Account/Login" -Method Post -Body '{"email": "test@test.com", "password": "TEST1234test!@!"}' -ContentType "application/json"
$token = $loginToken[0].token
echo  "token : $token"
$foodlikes = Invoke-WebRequest -Uri "http://localhost:88/api/FoodLike/Get/" -Headers @{ "Authorization"="Bearer $token" } -Method GET  -ContentType "application/json"
echo  "foodlikes : $foodlikes"

$id = Invoke-RestMethod -Uri "http://localhost:88/api/FoodLike/Create/" -Headers @{ "Authorization"="Bearer $token" } -Method Post -Body '{"day": "948170001", "meal": "948170000", "foodid": "54076A9D-5E74-E111-BB4F-00155D0A0C06"}' -ContentType "application/json"
$foodLikeId = $id[0].id

echo  "foodlikeId : $foodLikeId"
$updateParams = @{day ="948170001";meal="948170000";foodlikeid=$foodLikeId}
echo $updateParams
$params =  ConvertTo-Json $updateParams
echo $params 
$update = Invoke-WebRequest -Uri "http://localhost:88/api/FoodLike/Update/" -Headers @{ "Authorization"="Bearer $token" } -Method Patch -Body $params  -ContentType "application/json"
echo  "foodlike update : $update"
$delete = Invoke-WebRequest -Uri "http://localhost:88/api/FoodLike/Delete/$foodLikeId" -Headers @{ "Authorization"="Bearer $token" } -Method Delete -ContentType "application/json"
echo  "foodlike delete : $delete"
