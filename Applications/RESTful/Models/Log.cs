﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTful.Models
{
    public class Log
    {
        private String stackTrace;

        public Log()
        {
            LogId = Guid.NewGuid();
        }

        public Guid LogId { get; set; }

        public String Source { get; set; }

        public String Message { get; set; }

        public String StackTrace
        {
            get
            {
                return (stackTrace);
            }
            set { stackTrace = value; }
        }

        public DateTime CreatedOn {get;set;}
    }
}
