﻿using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTful.Helpers
{
    public class Globals
    {
        public static bool IsLogEnabled { get; set; }

        public static char Separator { get; set; }

        public static LogHelperBase LogHelper { get; set; }

        public static OrganizationServiceProxy CRMService {get;set;}
    }
}
