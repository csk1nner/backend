﻿using Microsoft.Xrm.Sdk;
using NutriStyle.WebAPI.RESTful.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTful.Helpers
{
    public class ConvertFrom
    {

        public dynamic Execute<T>(T entity)
        {
            Entity crmEntity = null;

            if (entity is Entity)
            {
                crmEntity = (Entity)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
            }

            if (entity is User)
            {
                var obj = (User)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
                var dr = new Entity("contact")
                {
                    Id = obj.UserId,
                    ["firstname"] = obj.FirstName,
                    ["lastname"] = obj.LastName,
                    ["emailaddress1"] = obj.EmailAddress1,
                };
                return (dr);
            }

            else if (entity is Entity && crmEntity.LogicalName.Equals("contact"))
            {
                //var obj = (sm_DiscoveryRequest)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
                var dr = new User()
                {
                    UserId = crmEntity.Id,
                    FirstName = (String)crmEntity["firstname"],
                    LastName = (String)crmEntity["lastname"],
                    EmailAddress1 = (String)crmEntity["emailaddress1"]
                };
                return (dr);
            }

            return (default(T));
        }

    }
}
