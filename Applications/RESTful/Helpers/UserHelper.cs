﻿using Microsoft.Xrm.Sdk.Query;
using NutriStyle.WebAPI.RESTful.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace NutriStyle.WebAPI.RESTful.Helpers
{
    public class UserHelper
    {

        public User LoginUser(String email, String password)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                var results = xmlDoc.CreateNode(XmlNodeType.Element, "results", "");
                var valid = xmlDoc.CreateNode(XmlNodeType.Element, "valid", "");
                var guid = xmlDoc.CreateNode(XmlNodeType.Element, "guid", "");

                xmlDoc.AppendChild(results);
                results.AppendChild(valid);
                results.AppendChild(guid);

                valid.InnerText = false.ToString();
                guid.InnerText = Guid.Empty.ToString();

                var contactFetchXml = @"<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0' >
                        <entity name='contact'> 
                            <attribute name='firstname'/>
                            <attribute name='emailaddress1'/> 
                            <attribute name='lastname'/> 
                            <attribute name='gendercode'/>
                            <attribute name='dc_targetweight'/>
                            <attribute name='dc_currentweight'/>
                            <attribute name='dc_heightfeet'/>
                            <attribute name='dc_heightinches'/>
                            <attribute name='birthdate'/>
                            <attribute name='dc_activitylevel'/>
                            <attribute name='dc_poundsperweek'/>
                            <attribute name='dc_maintaintargetweight'/>
                            <attribute name='dc_kcalcalculatedtarget'/>
                            <attribute name='dc_weightkg'/> 
                            <attribute name='dc_heightcm'/> 
                            <attribute name='dc_age'/>
                            <attribute name='dc_bmi'/>
                            
                            <attribute name='contactid'/>

                            <attribute name='dc_morningsnack'/>
                            <attribute name='dc_afternoonsnack'/>
                            <attribute name='dc_eveningsnack'/>
                            <attribute name='dc_menupresetid'/>
                            <attribute name='dc_dee'/>
                            <attribute name='dc_userspecifiedkcaltarget'/>
                            <attribute name='dc_kcaltarget'/>
                            
                            <attribute name='dc_rollshoppinglisttoparent'/>
                            <filter type='and'>
                                <condition attribute='emailaddress1' value='@EMAIL' operator='eq'/>
                                <condition attribute='dc_password' value='@PASSWORD' operator='eq'/>
                            </filter>
                            <link-entity name='dc_menu' alias='dc_menu' to='contactid' from='dc_contactid' link-type='outer'>
                                 <attribute name='dc_menuid'/>
                                <filter type='and'> <condition attribute='dc_primarymenu' value='1' operator='eq'/> 
                                </filter> 
                            </link-entity>
                        </entity> 
                    </fetch>";

                contactFetchXml = contactFetchXml.Replace("@EMAIL", email);
                contactFetchXml = contactFetchXml.Replace("@PASSWORD", password);

                XmlDocument fetchXmlDoc = new XmlDocument();
                fetchXmlDoc.LoadXml(contactFetchXml);

                var response = Globals.CRMService.RetrieveMultiple(new FetchExpression(contactFetchXml));

                Globals.LogHelper.CreateLog("Found " + response.Entities.Count + " contact records");
                if (response != null && response.Entities.Count > 0)
                {
                    var contact = response.Entities[0];
                    var cf = new ConvertFrom();
                    var user = cf.Execute(contact);
                    return (user);
                }


            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("UserHelper.cs: LoginUser(String, String)", err);
            }
        }


    }
}
