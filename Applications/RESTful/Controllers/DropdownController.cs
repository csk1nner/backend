﻿using Microsoft.AspNetCore.Mvc;
using System;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;

namespace NutriStyle.WebAPI.RESTful.Controllers
{
    //https://stackoverflow.com/questions/9110724/serializing-a-list-to-json

    [Route("api/[controller]")]
    public class DropdownController : BaseController
    {
        /// <summary>
        /// Returns the KeyValuePair collection of Height Feet values that are on the contact record
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetHeightFeet")]
        public IActionResult GetHeightFeet()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveHeightFeet();
               
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetHeightFeet", err);
            }
            return Error("DropdownController.cs: GetHeightFeet()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of Height Inches values that are on the contact record
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetHeightInches")]
        public IActionResult GetHeightInches()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveHeightInches();
               
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetHeightInches", err);
            }
            return Error("DropdownController.cs: GetHeightInches()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of GenderCode values that are on the contact record
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetGender")]
        public IActionResult GetGender()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveGender();
                
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetGender()", err);
            }
            return Error("DropdownController.cs: GetGender()");
        }

        /// <summary>
        /// Returns the KeyValuePair collection of Meal type values that are on the dc_meal global picklist
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetMealTypes")]
        public IActionResult GetMealTypes()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveMealTypes();
                return(Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetMealTypes()", err);
            }
            return Error("DropdownController.cs: GetMealTypes()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of day type values that are on the dc_day global picklist
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetDayTypes")]
        public IActionResult GetDayTypes()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveDayTypes();
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetDayTypes()", err);
            }
            return Error("DropdownController.cs: GetDayTypes()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of dc_country entities that are  active
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetCountries")]
        public IActionResult GetCountries()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveCountries();
                
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetCountries", err);
            }
            return Error("DropdownController.cs: GetCountries()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of dc_grocers entities that are active
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetGrocers")]
        public IActionResult GetGrocers()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveGrocers();
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetGrocers()", err);
            }
            return Error("DropdownController.cs: GetGrocers()");
        }
        /// <summary>
        /// Returns the KeyValuePair collection of account types.  This is currently not  mapped to anything in the backend
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetAccountTypes")]
        public IActionResult GetAccountTypes()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveAccountTypes();
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetAccountTypes", err);
            }
            return Error("DropdownController.cs: GetAccountTypes()");
        }
        
        /// <summary>
        /// Retrieve the active preset records.  The filter is an int and fuctions as follows:  There are  five
        /// filter types: None, Vegan, Vegeterian,  Gluten Free and Drairy Free.  The on/off are represented by a 1 or zero
        /// so 00000 is all off.  10000 is None, 01000 is Vegan,  etc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetMenuPresets/{filter}")]
        public IActionResult GetMenuPresets(String filter)
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrievePresets(filter);
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetMenuPresets()", err);
            }
            return Error("DropdownController.cs: GetMenuPresets()");
        }
        /// <summary>
        /// Reurns the verify customers records.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetVerifyCustomers")]
        public IActionResult GetVerifyCustomers()
        {
            try
            {
                var dh = new DropdownHelper();
                var list = dh.RetrieveVerifyCustomers();
                return (Ok(list));
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownController.cs: GetVerifyCustomers()", err);
            }
            return Error("DropdownController.cs: GetVerifyCustomers()");
        }

    }
}
