﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using JWT;
using JWT.Serializers;
using JWT.Algorithms;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using NutriStyle.WebAPI.RESTfulModels;
using Swashbuckle.AspNetCore.Examples;
using NutriStyle.WebAPI.RESTfulModels.Examples;
using Newtonsoft.Json;

namespace NutriStyle.WebAPI.RESTful.Controllers
{
    //https://auth0.com/blog/asp-dot-net-core-authentication-tutorial/
    //https://auth0.com/blog/securing-asp-dot-net-core-2-applications-with-jwts/
    //https://stormpath.com/blog/tutorial-policy-based-authorization-asp-net-core
    //http://blog.getpostman.com/2014/01/27/extracting-data-from-responses-and-chaining-requests/

    [Route("api/[controller]")]
    public class UserController : BaseController
    {

        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly JWTSettings options;
        private IConfiguration config;
               
        public UserController(IConfiguration config)
        {
            this.config = config;
        }

        /// <summary>
        /// Update the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="400">If update fails</response>    
        [SwaggerRequestExample(typeof(User), typeof(UserExample))]
        [HttpPatch("Update"), Authorize]
        public IActionResult Update([FromBody] User user)
        {
            try
            {
                var uh = new UserHelper();
                var result = uh.Update(user);
                var response = Ok(result);
                return response;
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("", err);
            }

            return Error("Unexpected error");
        }

        /// <summary>
        /// Retrieve the user for the passed in email and password.
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [HttpPost("Retrieve"), Authorize]
        public IActionResult Retrieve([FromBody] Account credentials)
        {
            var uh = new UserHelper();
            var user = uh.LoginUser(credentials.Email, credentials.Password);
            var json = JsonConvert.SerializeObject(user);
            var response = Ok(json);
            return response;
        }
       
    }
}
