﻿using Microsoft.AspNetCore.Mvc;
using System;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using NutriStyle.WebAPI.RESTfulModels;
using Swashbuckle.AspNetCore.Examples;
using NutriStyle.WebAPI.RESTfulModels.Examples;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Linq;

namespace NutriStyle.WebAPI.RESTful.Controllers
{

    [Route("api/[controller]")]
    public class FoodlikeController : BaseController
    {
        private IConfiguration config;
               
        public FoodlikeController(IConfiguration config)
        {
            this.config = config;
        }

        /// <summary>
        /// Retrieve the food likes for the user.  UserId is provided by Session management
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("Get"), Authorize]
        public IActionResult Retrieve()
        {
            try
            {
                var currentUser = HttpContext.User;

                if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                {
                    var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                    var flh = new FoodLikeHelper();
                    var list = flh.RetrieveFoodLikes(userId);
                    var response = Ok(list);
                    return response;
                }
                return Error("FoodLikeController.cs: Retrieve(): UserId value is Guid.Empty");
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeController.cs: Retrieve()", err);
            }

            return Error("FoodLikeController.cs: Retrieve()");
        }

        /// <summary>
        /// Returns Guid of the created food like.  Writes over the UserId attribute with the value from the  claim
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        /// <response code="200">Returns Guid</response>
        /// <response code="400">If create fails</response>    
        
        [HttpPost("Create"), Authorize]
        [SwaggerRequestExample(typeof(FoodLike), typeof(FoodLikeExample))]
        public IActionResult Create([FromBody] FoodLike fl)
        {
            var flh = new FoodLikeHelper();
            try
            {
                if (fl != null)
                {
                    var currentUser = HttpContext.User;

                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                        fl.UserId = userId;
                        var Id = flh.CreateFoodLike(fl);
                        var response = Ok(new { Id });
                        return response;
                    }
                    return Error("FoodLikeController.cs: Create(FoodLike): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeController.cs: Create(FoodLike)", err);
                return Error("FoodLikeController.cs: Create(FoodLike): " + err.Message);
            }
            return Error("FoodLikeController.cs: Create(FoodLike): Unknown error");
        }
        /// <summary>
        /// Update the foodlike
        /// </summary>
        /// <param name="foodlike"></param>
        /// <returns></returns>
        /// /// <response code="200">Returns bool based on success or failure</response>
        /// <response code="400">If update fails</response>    
        [HttpPatch("Update"), Authorize]
        [SwaggerRequestExample(typeof(FoodLike), typeof(FoodLikeExample))]
        public IActionResult Update([FromBody] FoodLike fl)
        {
            var flh = new FoodLikeHelper();
            try
            {
                if (fl != null)
                {
                    var currentUser = HttpContext.User;

                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                        fl.UserId = userId;
                        var result = flh.UpdateFoodLike(fl);
                        var response = Ok(new { result });
                        return response;
                    }
                    return Error("FoodLikeController.cs: Upudate(FoodLike): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeController.cs: Upudate(FoodLike)", err);
                return Error("FoodLikeController.cs: Update(FoodLike): " + err.Message);
            }
            return Error("FoodLikeController.cs: Update(FoodLike): Unknown error");
        }

        /// <summary>
        /// Delete the foodlike the passed in Guid represents
        /// </summary>
        /// <param name="foodLikeId"></param>
        /// <returns></returns>
        /// <response code="200">Returns bool based on success or failure</response>
        /// <response code="400">If delete fails</response>    
        [HttpDelete("Delete/{foodlikeid}"), Authorize]
        public IActionResult Delete(Guid foodLikeId)
        {
            var flh = new FoodLikeHelper();
            try
            {
                if (foodLikeId != Guid.Empty)
                {
                    var currentUser = HttpContext.User;

                    //Todo: Validate that the user owns the foodlike before deleting
                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                       
                        var result = flh.DeleteFoodLike(foodLikeId);
                        var response = Ok(new { result });
                        return response;
                    }
                    return Error("FoodLikeController.cs: Delete(FoodLike): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeController.cs: Delete(Guid)", err);
                return Error("FoodLikeController.cs: Delete(Guid): " + err.Message);
            }
            return Error("FoodLikeController.cs: Delete(Guid): Unknown error");
        }

    }
}
