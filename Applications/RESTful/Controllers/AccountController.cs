﻿using Microsoft.AspNetCore.Mvc;
using System;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Examples;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Collections.Generic;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Microsoft.AspNetCore.Identity;
using NutriStyle.WebAPI.RESTfulModels;
using NutriStyle.WebAPI.RESTfulModels.Examples;
using Microsoft.AspNetCore.Http;
using NutriStyle.WebAPI.RESTful.Extensions;
using System.Security.Claims;

namespace NutriStyle.WebAPI.RESTful.Controllers
{

    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private IConfiguration config;
        private readonly JWTSettings options;

        public AccountController(IConfiguration config)
        {
            this.config = config;
        }

        /// <summary>
        /// Check to see if the email exists
        /// </summary>
        /// <returns>bool</returns>
        /// <response code="200">bool</response>
        /// <response code="400">If endpoint fails</response>    
        [AllowAnonymous]
        [HttpGet("GetDoesEmailExist/{email}")]
        public IActionResult GetDoesEmailExist(String email)
        {
            try
            {
                var uh = new UserHelper();
                var doesExist = uh.DoesEmailExist(email);
                //return Json(doesExist);
                var response = Ok(new {  doesExist });
                return (response);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("AccountController.cs: GetDoesEmailExist(String)", err);
            }
            return Error("AccountController.cs: GetDoesEmailExist(String)");
        }

        /// <summary>
        /// Returns a JWT token if successful.  Checks for the existance of the email before the user is created.  
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        /// <response code="200">Returns JWT token</response>
        /// <response code="400">If create fails</response>    
        [AllowAnonymous]
        [HttpPost("Create")]
        [SwaggerRequestExample(typeof(Account), typeof(AccountExample))]
        public IActionResult Create([FromBody] Account credentials)
        {
            IActionResult response = Unauthorized();
            var uh = new UserHelper();
            if (!uh.DoesEmailExist(credentials.Email))
            {
                if (credentials != null)
                {
                    var user = uh.CreateUser(credentials.Email, credentials.Password, String.Empty, String.Empty, String.Empty,
                        Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty, String.Empty, Guid.Empty);

                    var token = BuildToken(credentials, user.UserId);
                    response = Ok(new { token });

                }
                return response;
            }
        return Error("Email is in use");
        }


        /// <summary>
        /// Returns a JWT token if the login is successful
        /// </summary>
        /// <param name="credentials">json array containing email and password: {"email": "test@test.com", "password": "TEST1234test!@!"}</param>
        /// <returns></returns>
        /// <response code="200">Returns JWT token</response>
        /// <response code="400">If login fails</response>    
        [AllowAnonymous]
        [HttpPost("Login")]
        [SwaggerRequestExample(typeof(Account), typeof(AccountExample))]
        public IActionResult Login([FromBody] Account credentials)
        {
            IActionResult response = Unauthorized();

            {
                if (credentials != null)
                {
                    var uh = new UserHelper();
                    var user = uh.LoginUser(credentials.Email, credentials.Password);
                                      
                    if (user != null)
                    {
                        //session.Set<Guid>(Globals.SESSION_USERID, user.UserId);

                        var token = BuildToken(credentials, user.UserId);
                        response = Ok( new { token });
                        return response;
                    }
                    //return Error("User not found");
                }
               
            }
            return Error("Login failed");
        }

        private string BuildToken(Account credentials, Guid userId)
        {
            var key     = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"]));
            var creds   = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            
            var claims = new[] {
                new Claim(ClaimTypes.PrimarySid, userId.ToString()),
                new Claim(ClaimTypes.Role, 0.ToString()),
            };
            
            var token = new JwtSecurityToken(config["Jwt:Issuer"],
                config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string GetIdToken(IdentityUser user)
        {
            var payload = new Dictionary<string, object>
            {
                { "id", user.Id },
                { "sub", user.Email },
                { "email", user.Email },
                { "emailConfirmed", user.EmailConfirmed },
            };
            return GetToken(payload);
        }

        private string GetAccessToken(string Email)
        {
            var payload = new Dictionary<string, object>
            {
                { "sub", Email },
                { "email", Email }
            };
            return GetToken(payload);
        }

        private string GetToken(Dictionary<string, object> payload)
        {
            var secret = options.SecretKey;
            payload.Add("iss", options.Issuer);
            payload.Add("aud", options.Audience);
            payload.Add("nbf", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("iat", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("exp", ConvertToUnixTimestamp(DateTime.Now.AddDays(7)));
            var algorithm = new HMACSHA256Algorithm();
            var serializer = new JsonNetSerializer();
            var urlEncoder = new JwtBase64UrlEncoder();
            var encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            return encoder.Encode(payload, secret);
        }

        private static double ConvertToUnixTimestamp(DateTime date)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

    }
}
