﻿using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTfulProxy
{
    public class Utilities
    {

        public void Proxy()
        {
            //var nu = new NutriStyle.Dynamics.XRM.OrganizationServiceClient();

            var nu = new Nutristyle.WebAPI.XRM.Service();
            
            //return (nu);
        }


        public OrganizationServiceProxy GenerateProxy()
        {
            String user = "crmadmin";
            String password = "P@SSW0RD";
            String domain = "BitAssassins";
            String hostname = "stagingcrm";
            String orgName = "NS";
            String portnumber = "5555";

            var u = new Utilities();
            var errorString = String.Empty;

            var crmService = u.CreateCrmService(user, password, domain, hostname,
                orgName, Convert.ToInt32(portnumber), out errorString);

            return (crmService);

        }


        public OrganizationServiceProxy CreateCrmService(String username, String password, String domain,
            String server, String instanceName, int portNumber, out string errorMessage)
        {
            try
            {
                var credentials = new ClientCredentials();
                credentials.Windows.ClientCredential = new System.Net.NetworkCredential(username, password, domain);

                Uri organizationUri = new Uri("http://" + server + ":" + portNumber + "/" + instanceName + "/XRMServices/2011/Organization.svc");

                if (server.Contains("http") || server.Contains("https"))
                {
                    organizationUri = new Uri(server + ":" + portNumber + "/" + instanceName + "/XRMServices/2011/Organization.svc");
                }

                Uri homeRealmUri = null;
                OrganizationServiceProxy orgService = new OrganizationServiceProxy(organizationUri, homeRealmUri, credentials, null);

                //TODO: Comment your code!!!
                orgService.EnableProxyTypes();



                errorMessage = null;
                return (orgService);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred in procedure " + "Targe: " + ex.TargetSite + " Message: " + ex.Message + " Trace: " + ex.StackTrace;
                return null;
            }
        }
    }
}
