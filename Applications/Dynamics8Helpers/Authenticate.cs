﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DynamicConnections.Nutristyle.Dynamics8Helpers
{
    //https://msdn.microsoft.com/en-US/library/mt770369.aspx

    public class Authenticate
    {
        private HttpClient httpClient;
        private Version webAPIVersion = new Version(8, 0);

        private string getVersionedWebAPIPath()
        {
            return string.Format("v{0}/", webAPIVersion.ToString(2));
        }

        public async Task getWebAPIVersion()
        {

            HttpRequestMessage RetrieveVersionRequest =
              new HttpRequestMessage(HttpMethod.Get, getVersionedWebAPIPath() + "RetrieveVersion");

            HttpResponseMessage RetrieveVersionResponse =
                await httpClient.SendAsync(RetrieveVersionRequest);
            if (RetrieveVersionResponse.StatusCode == HttpStatusCode.OK)  //200
            {
                JObject RetrievedVersion = JsonConvert.DeserializeObject<JObject>(
                    await RetrieveVersionResponse.Content.ReadAsStringAsync());
                //Capture the actual version available in this organization
                webAPIVersion = Version.Parse((string)RetrievedVersion.GetValue("Version"));
            }
            else
            {
                /*
                Console.WriteLine("Failed to retrieve the version for reason: {0}",
                    RetrieveVersionResponse.ReasonPhrase);
                    */

                //throw new CrmHttpResponseException(RetrieveVersionResponse.Content);
            }

        }

        public HttpClient GetNewHttpClient(string userName, string password, string domainName, string webAPIBaseAddress)
        {
            //AuthenticationContext AuthContext = new AuthenticationContext(Authority, false);

            HttpClient client = new HttpClient(new HttpClientHandler()
            {
                Credentials = new NetworkCredential(userName, password, domainName)
            });
            client.BaseAddress = new Uri(webAPIBaseAddress);
            client.Timeout = new TimeSpan(0, 2, 0); // 2 minutes

            client.BaseAddress = new Uri(webAPIBaseAddress + "/api/data/");

            //http://dynamicscrmcoe.com/web-api-next-big-thing-developers-part-2/
            client.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            client.DefaultRequestHeaders.Add("OData-Version", "4.0");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthResult.AccessToken);
            return client;
        }

        public HttpClient RetrieveNewHttpClient()
        {
            var httpClient = GetNewHttpClient("crmadmin", "P@ssw0rd", "DC", "http://crmdev.dynamiconnections.com:5555/NS");
            return (httpClient);
        }

            /*
            private void ConnectToCRM(String[] cmdargs)
            {
                //Create a helper object to read app.config for service URL and application 
                // registration settings.
                Configuration config = null;
                if (cmdargs.Length > 0)
                { config = new FileConfiguration(cmdargs[0]); }
                else
                { config = new FileConfiguration(null); }
                //Create a helper object to authenticate the user with this connection info.
                Authentication auth = new Authentication(config);
                //Next use a HttpClient object to connect to specified CRM Web service.
                httpClient = new HttpClient(auth.ClientHandler, true);
                //Define the Web API base address, the max period of execute time, the 
                // default OData version, and the default response payload format.
                httpClient.BaseAddress = new Uri(config.ServiceUrl + "api/data/");
                httpClient.Timeout = new TimeSpan(0, 2, 0);
                httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
                httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
                httpClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
            */

        }
}
