﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DynamicConnections.Nutristyle.Dynamics8Helpers
{
    public class CRUD
    {
        //http://dynamicscrmcoe.com/web-api-next-big-thing-developers-part-2/


        /// <summary>
        /// Create account
        /// </summary>
        /// <returns></returns>
        public async Task<Guid> Create(HttpClient httpClient)
        {
            //var Id = Guid.NewGuid();

            var accountRecord = new JObject
                    {
                        {"name", "SMSMT"},
                        {"telephone1", "111-111-1111"}
                    };

            var execute = new DynamicsConnection();
            var Id = await execute.Create(DynamicsConnection.RegardingTypeCode.Account, accountRecord);
            return (Id);
        }
        public async Task<bool> Update(HttpClient httpClient, Guid Id)
        {
            //var Id = Guid.NewGuid();

            var accountRecord = new JObject
                    {
                        {"name", "SMSMT - Update"},
                        {"telephone1", "222-222-2222"}
                    };

            var execute = new DynamicsConnection();
            var result = await execute.Update(DynamicsConnection.RegardingTypeCode.Account, Id, accountRecord);
            return (result);
        }

        public async void FetchXML()
        {

            string fetchXmlQuery =
                "<fetch mapping='logical' output-format='xml-platform' version='1.0' distinct='false'>" +
                  "<entity name ='account'>" +
                    "<attribute name ='name' />" +
                    "<attribute name ='telephone1' />" +
                    "<order descending ='true' attribute='name' />" +
                  "</entity>" +
                "</fetch>";

            var execute = new DynamicsConnection();
            var result = execute.FetchXML(DynamicsConnection.RegardingTypeCode.Account, fetchXmlQuery).Result;
            //var cf = new ConvertFrom();
            var x = 0;
            x++;
        }

        public async void QueryMetadata()
        {
            try
            {
                var execute = new DynamicsConnection();
                //var result = execute.QueryPicklist(DynamicsConnection.RegardingTypeCode.Contact, "gendercode");

            }
            catch (Exception err)
            {
                //Globals.LogHelper.CreateLog("", err);
                throw new Exception("Error with metadata query", err);
            }
            
        }

        /*
        public static Task<HttpResponseMessage> SendAsJsonAsync<T>(HttpClient client, HttpMethod method, string requestUri, T value)
        {
            var content = value.GetType().Name.Equals("JObject") ?
                value.ToString() :
                JsonConvert.SerializeObject(value, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Ignore });

            var request = new HttpRequestMessage(method, requestUri) { Content = new StringContent(content) };
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            return client.SendAsync(request);
        }*/
    }

}
