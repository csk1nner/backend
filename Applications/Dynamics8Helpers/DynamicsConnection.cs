﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DynamicConnections.Nutristyle.Dynamics8Helpers
{
    /// <summary>
    /// URLs always have the trailing '/' - So http://xyz.com/NS/ instead of http://xyz.com/NS
    /// </summary>
    public class DynamicsConnection
    {

        public enum RegardingTypeCode
        {
            Account, Contact, Preset, Country, Grocer, VerifyCustomer,  FoodLike, FoodDislike,  Meals
        };

        private String RetrieveVersion()
        {
            return ("v8.2/");
        }
        private String BuildURL(RegardingTypeCode entityTypeCode, String requestUri)
        {
            switch (entityTypeCode)
            {

                case RegardingTypeCode.Account:
                    {
                        requestUri = requestUri + "accounts";
                    };
                    break;
                case RegardingTypeCode.Contact:
                    {
                        requestUri = requestUri + "contacts";
                    };
                    break;
                case RegardingTypeCode.Preset:
                    {
                        requestUri = requestUri + "dc_presetses";
                    };
                    break;
                case RegardingTypeCode.Country:
                    {
                        requestUri = requestUri + "dc_countries";
                    };
                    break;
                case RegardingTypeCode.Grocer:
                    {
                        requestUri = requestUri + "dc_grocers";
                    };
                    break;
                case RegardingTypeCode.VerifyCustomer:
                    {
                        requestUri = requestUri + "dc_verifycustomers";
                    };
                    break;
                case RegardingTypeCode.FoodLike:
                    {
                        requestUri = requestUri + "dc_foodlikes";
                    };
                    break;
                case RegardingTypeCode.FoodDislike:
                    {
                        requestUri = requestUri + "dc_fooddislikes";
                    };
                    break;
                case RegardingTypeCode.Meals:
                    {
                        requestUri = requestUri + "dc_meals";
                    };
                    break;
            }
            return (requestUri);
        }

        /// <summary>
        /// Creates a record.  Returns the Id from the create action
        /// </summary>
        /// <param name="entityTypeCode"></param>
        /// <param name="record"></param>
        /// <returns></returns>
        public async Task<Guid> Create(RegardingTypeCode entityTypeCode, JObject record)
        {
            var a = new Authenticate();
            var httpClient = a.RetrieveNewHttpClient();

            var requestUri = RetrieveVersion();

            requestUri = BuildURL(entityTypeCode, requestUri);

            var createRequest = new HttpRequestMessage(HttpMethod.Post, requestUri);//"v8.0/accounts");
            createRequest.Content = new StringContent(record.ToString(), Encoding.UTF8, "application/json");
            var createResponse = await httpClient.SendAsync(createRequest);

            var Id = new Guid();
            if (createResponse.IsSuccessStatusCode &&
                createResponse.StatusCode == HttpStatusCode.NoContent)
            {
                var recordUri = createResponse.Headers.GetValues("OData-EntityId").FirstOrDefault();

                if (recordUri != null)
                {
                    Id = Guid.Parse(recordUri.Split('(', ')')[1]);
                }
                return (Id);
            }

            return (Guid.Empty);

        }
        /// <summary>
        /// Only tansmite the attributes that need to be updated
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<bool> Update(RegardingTypeCode entityTypeCode, Guid Id, JObject entity)
        {
            var a = new Authenticate();
            var httpClient = a.RetrieveNewHttpClient();
            var requestUri = RetrieveVersion();
            requestUri = BuildURL(entityTypeCode, requestUri);

            requestUri = requestUri + "(" + Id.ToString() + ")";

            var updateRequest = new HttpRequestMessage(new HttpMethod("PATCH"), requestUri);
            updateRequest.Content = new StringContent(entity.ToString(), Encoding.UTF8, "application/json");
            var updateResponse = await httpClient.SendAsync(updateRequest);
            if (updateResponse.StatusCode == HttpStatusCode.NoContent) //204  
            {
                return (true);
            }
            return (false);
        }

        //https://msdn.microsoft.com/en-us/library/mt607664.aspx
        /// <summary>
        /// Deletes the CRM record that the Enum and Id (Guid) points at
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Delete(RegardingTypeCode entityTypeCode, Guid Id)
        {
            var a = new Authenticate();
            var httpClient = a.RetrieveNewHttpClient();

            var requestUri = RetrieveVersion();
           
            requestUri = BuildURL(entityTypeCode, requestUri);

            requestUri = requestUri + "(" + Id.ToString() + ")";
            var deleteResponse = await httpClient.DeleteAsync(requestUri);

            if(deleteResponse.IsSuccessStatusCode && 
                deleteResponse.StatusCode == HttpStatusCode.NoContent) //response.StatusCode == HttpStatusCode.NoContent
            {
                return (true);
            }
            return (false);
        }

        /// <summary>
        /// Retrieve the option set values for the passed in entityId and attributeId
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public async Task<List<KeyValuePair<String, String>>> QueryPicklist(Guid entityId, Guid attributeId)
        {
            try
            {
                var a = new Authenticate();
                var httpClient = a.RetrieveNewHttpClient();
              
                //Values are pulled from the metadata.  Using the database as it is much faster than trying to browse through APIs
                                
                var requestUri = RetrieveVersion();

                if (entityId != Guid.Empty && attributeId != Guid.Empty)
                {
                    requestUri = requestUri + "EntityDefinitions(" + entityId + ")/Attributes(" + attributeId + ")/Microsoft.Dynamics.CRM.PicklistAttributeMetadata?$select=LogicalName&$expand=OptionSet";

                    var getResponse = await httpClient.GetAsync(requestUri);
                    var createGet = new HttpRequestMessage(HttpMethod.Get, requestUri);

                    if (getResponse.IsSuccessStatusCode &&
                        getResponse.StatusCode == HttpStatusCode.OK)
                    { 
                        var collection = JsonConvert.DeserializeObject<JObject>(getResponse.Content.ReadAsStringAsync().Result);
                        var list = collection["OptionSet"]["Options"];
                        var dropdownItems = new List<KeyValuePair<String, String>>();
                        foreach (var x in (JArray)list)
                        {
                            var optionId = (String)x["Value"];
                            var label = (String)x["Label"]["LocalizedLabels"].First.First.First;
                            dropdownItems.Add(new KeyValuePair<String, String>(optionId, label));
                        }
                        return (dropdownItems);
                    }
                }
            }
            catch (Exception err)
            {
                //Globals.LogHelper.CreateLog("DynamicsConnection.cs: QueryPicklist(RegardingTypeCode, String)", err);
                throw new Exception("Error with query of metadata", err);
            }
            return (null);
        }

        /// <summary>
        /// Retrieve the option set values for the passed in attributeId.  For Global picklists
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public async Task<List<KeyValuePair<String, String>>> QueryPicklist(Guid attributeId)
        {
            try
            {
                var a = new Authenticate();
                var httpClient = a.RetrieveNewHttpClient();

                //Values are pulled from the metadata.  Using the database as it is much faster than trying to browse through APIs

                var requestUri = RetrieveVersion();

                if (attributeId != Guid.Empty)
                {
                    requestUri = requestUri + "GlobalOptionSetDefinitions("+attributeId+")";

                    var getResponse = await httpClient.GetAsync(requestUri);
                    var createGet = new HttpRequestMessage(HttpMethod.Get, requestUri);

                    if (getResponse.IsSuccessStatusCode &&
                        getResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var collection = JsonConvert.DeserializeObject<JObject>(getResponse.Content.ReadAsStringAsync().Result);
                        var list = collection["Options"];
                        var dropdownItems = new List<KeyValuePair<String, String>>();
                        foreach (var x in (JArray)list)
                        {
                            var optionId = (String)x["Value"];
                            var label = (String)x["Label"]["LocalizedLabels"].First.First.First;
                            dropdownItems.Add(new KeyValuePair<String, String>(optionId, label));
                        }
                        return (dropdownItems);
                    }
                }
            }
            catch (Exception err)
            {
                //Globals.LogHelper.CreateLog("DynamicsConnection.cs: QueryPicklist(RegardingTypeCode, String)", err);
                throw new Exception("Error with query of metadata", err);
            }
            return (null);
        }



        public async Task<JObject> Aggregate(RegardingTypeCode entityTypeCode, String fetchXml)
        {
            return(FetchXML(entityTypeCode, fetchXml).Result);
        }


        /// <summary>
        /// Returns the entities from the fetchxml of RegardingTypeCode
        /// </summary>
        /// <param name="entityTypeCode"></param>
        /// <param name="fetchXml"></param>
        public async Task<JObject> FetchXML(RegardingTypeCode entityTypeCode, String fetchXml)
        {
            var a = new Authenticate();
            var httpClient = a.RetrieveNewHttpClient();

            var requestUri = RetrieveVersion();
           
            requestUri = BuildURL(entityTypeCode, requestUri);

            requestUri = requestUri + "?fetchXml=" + WebUtility.UrlEncode(fetchXml);

            var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
            request.Headers.Add("Prefer", "odata.maxpagesize=" + 5000.ToString()); //+ maxPageSize.ToString());
            /*
            if (true)//(formatted)
            {
                request.Headers.Add("Prefer", "odata.include-annotations=OData.Community.Display.V1.FormattedValue");
            }
            */

            var response = await httpClient.SendAsync(request);
            if (response.StatusCode == HttpStatusCode.OK) //200
            {
                var collection = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result);
                return (collection);

            }
            return (null);
        }



    }
}
