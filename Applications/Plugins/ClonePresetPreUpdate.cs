﻿using System;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Messages;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreUpdate message against the dc_preset entity.  This is part of the clone food feature that is 
    /// kicked off in the UI (Dynamics) by a javascript event firing when a button is clicked on.  Plugin also can be fired by setting
    /// the dc_clonepreset attribute to true
    /// </summary>
    public class ClonePresetPreUpdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);
                tracingService.Trace("ClonePresetPreUpdate: starting: " + pluginExecutionContext.PrimaryEntityName + ": " + pluginExecutionContext.MessageName);

                var preImage = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                var target = (Entity)pluginExecutionContext.InputParameters["Target"];

                if (target.Contains("dc_clonepreset"))
                {
                    tracingService.Trace("Check: value of dc_clonepreset: " + target["dc_clonepreset"]);

                    if ((Boolean)target["dc_clonepreset"])
                    {
                        target["dc_clonepreset"] = false;

                        Guid newId = Guid.NewGuid();
                        tracingService.Trace("Condition: dc_clonepreset is checked. Cloning the preset: " + preImage["dc_name"]);
                        //Need to create a new entity.
                        Entity clonePreset = crmService.Retrieve("dc_presets", preImage.Id, new ColumnSet(true));//This gets all the relationships

                        clonePreset["dc_presetsid"] = newId;
                        clonePreset.Id = newId;
                        preImage["dc_clonepreset"] = false;

                        clonePreset["dc_name"] = "Copy " + clonePreset["dc_name"];

                        crmService.Create(clonePreset);
                        tracingService.Trace("Cloned preset");

                        #region sub categories
                        //Find all the food likes
                        String fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='dc_component_template_sel'>
                            <all-attributes/>
                            <filter type='and'>
                              <condition attribute='dc_menupresetid' operator='eq' value='@ID' />
                            </filter>
                          </entity>
                        </fetch>";

                        fetchXml = fetchXml.Replace("@ID", preImage.Id.ToString());

                        EntityCollection collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (collection != null && collection.Entities.Count > 0)
                        {
                            foreach (Entity subCategory in collection.Entities)
                            {

                                Entity newSubCategory = subCategory;
                                newSubCategory["dc_menupresetid"] = new EntityReference("dc_presets", newId);
                                newSubCategory.Id = Guid.NewGuid();
                                newSubCategory["dc_component_template_selid"] = newSubCategory.Id;
                                crmService.Create(newSubCategory);
                                tracingService.Trace("Added subcategory: " + newSubCategory.Id);
                            }
                        }
                        #endregion

                        #region food likes
                        //Find all the food likes
                        fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='dc_foodlike'>
                            <all-attributes/>
                            <filter type='and'>
                              <condition attribute='dc_menupresetid' operator='eq' value='@ID' />
                            </filter>
                          </entity>
                        </fetch>";

                        fetchXml = fetchXml.Replace("@ID", preImage.Id.ToString());

                        collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (collection != null && collection.Entities.Count > 0)
                        {
                            foreach (Entity foodLike in collection.Entities)
                            {

                                Entity newFoodLike = foodLike;
                                newFoodLike["dc_menupresetid"] = new EntityReference("dc_presets", newId);
                                newFoodLike.Id = Guid.NewGuid();
                                newFoodLike["dc_foodlikeid"] = newFoodLike.Id;
                                crmService.Create(newFoodLike);
                                tracingService.Trace("Added food like: " + newFoodLike.Id);
                            }
                        } 
                        #endregion

                        #region food dislikes
                        //deal with MtM relationship
                        fetchXml = "<fetch mapping='logical' no-lock='true' > <entity name='dc_dc_presets_dc_meal_component'>"
                            + "<all-attributes />"
                            + "<filter>"
                            + "<condition attribute='dc_presetsid' operator='eq' value ='" + preImage.Id.ToString() + "' />"
                            + "</filter>"
                            + "</entity>"
                            + "</fetch>";

                        // Perform the query
                        collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (collection != null && collection.Entities.Count > 0)
                        {
                            foreach (Entity entity in collection.Entities)
                            {
                                if (entity.Contains("dc_meal_componentid"))
                                {
                                    Guid subCategoryId = (Guid)entity["dc_meal_componentid"];
                                    tracingService.Trace("subCategoryId: " + subCategoryId);
                                    //Create relationship
                                    AssociateRequest request = new AssociateRequest()
                                    {
                                        Target = new EntityReference("dc_presets", clonePreset.Id),
                                        RelatedEntities = new EntityReferenceCollection
                                        {
                                            new EntityReference("dc_meal_component", subCategoryId)
                                        },
                                        Relationship = new Microsoft.Xrm.Sdk.Relationship("dc_dc_presets_dc_meal_component")
                                    };
                                    crmService.Execute(request);
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        tracingService.Trace("Condition: dc_clone_food is not checked, not cloning the food");
                    }
                }
            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                tracingService.Trace("Code: " + ex.Detail.ErrorCode);
                tracingService.Trace("Message: " + ex.Detail.Message);
                tracingService.Trace("Trace: " + ex.Detail.TraceText);
                tracingService.Trace("Inner Fault: " + ex.Detail.InnerFault);
            }


            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }
        private static bool DoesRelationshipExist(IOrganizationService crmService, string relationshipSchemaName,
            string entity1SchemaName, Guid entity1KeyValue, string entity2SchemaName, Guid entity2KeyValue)
        {
            // Assemble FetchXML to query the intersection entity directly
            string fetchXml = "<fetch mapping='logical' no-lock='true' > <entity name='" + relationshipSchemaName + "'>"
            + "<all-attributes />"
            + "<filter>"
            + "<condition attribute='" + entity1SchemaName + "id' operator='eq' value ='" + entity1KeyValue.ToString() + "' />"
            + "<condition attribute='" + entity2SchemaName + "id' operator='eq' value='" + entity2KeyValue.ToString() + "' />"
            + "</filter>"
            + "</entity>"
            + "</fetch>";

            // Perform the query
            EntityCollection collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collection.Entities.Count == 0)
                return false;
            else
                return true;
        }
    }
}
