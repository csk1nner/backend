﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;

using DynamicConnections.NutriStyle.CRM2011.Plugins.Helpers;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PostCreate, PostUpdate & PostDelete message against the dc_ingredient entity.  Updates the dc_food (parent)
    /// as well.  Builds out a dc_food_nutrients entity for the dc_ingredient.
    /// </summary>
    public class IngredientRollupPostUpdate : IPlugin
    {
        //
        //dc_ingredient


        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                // get the execution context from the service provider temp code to makea  change
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);
                tracingService.Trace("Preset: starting: " + pluginExecutionContext.PrimaryEntityName + ":" + pluginExecutionContext.MessageName);

                // get the Entity postIngredient
                Entity postIngredient = new Entity();

                if (pluginExecutionContext.MessageName != "Delete")
                {
                    if (pluginExecutionContext.PostEntityImages.Count > 0)
                    {
                        postIngredient = (Entity)pluginExecutionContext.PostEntityImages["postimage"];
                        tracingService.Trace("Retrieved postimage.");
                    }
                }
                else
                {
                    if (pluginExecutionContext.PreEntityImages.Count > 0)
                    {
                        postIngredient = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                        tracingService.Trace("Retrieved preimage.");
                    }
                }
                
                Guid parentFoodId               = Guid.Empty;
                double numberOfServings         = 0d;
                double unitGramWeight           = 0d;
                double portionAmount            = 0d;

                if (postIngredient.Contains("dc_foodid"))
                {
                    parentFoodId = ((EntityReference)postIngredient["dc_foodid"]).Id;
                }
                tracingService.Trace("parentFoodId: " + parentFoodId.ToString());
                
                Entity parentFoodEntity = crmService.Retrieve("dc_foods", parentFoodId, new ColumnSet(new String[] {"dc_unit_gram_weight", "dc_numberofservings", "dc_portion_amount"}));

                unitGramWeight      = parentFoodEntity.Contains("dc_unit_gram_weight") ? (double)parentFoodEntity["dc_unit_gram_weight"] : 0d;
                numberOfServings    = parentFoodEntity.Contains("dc_numberofservings") ? (double)parentFoodEntity["dc_numberofservings"] : 0d;
                portionAmount       = parentFoodEntity.Contains("dc_portion_amount") ? (double)parentFoodEntity["dc_portion_amount"] : 0d;


                tracingService.Trace("unitGramWeight: " + unitGramWeight);
                //tracingService.Trace("numberOfServings: " + numberOfServings);

                var i = new Ingredient();
                i.Rollup(crmService, tracingService, parentFoodId, numberOfServings, portionAmount, true);

                //crmService.Update(parentFoodEntity);
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }

        

    }
}
