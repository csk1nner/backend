﻿using System;
using Microsoft.Xrm.Sdk;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PostDelete message against the dc_foods entity.  
    /// Delete related food nutrient when the dc_foods entity gets deleted. 
    /// </summary>
    public class FoodsPostDelete : IPlugin
    {
        //
        
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // get the execution context from the service provider temp code to makea  change
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);
                tracingService.Trace("FoodsPostDelete: starting: " + pluginExecutionContext.PrimaryEntityName + ": " + pluginExecutionContext.MessageName);

                //Make sure image is there
                Entity preImage = null;
                if (pluginExecutionContext.PreEntityImages.Count > 0 && pluginExecutionContext.PreEntityImages["preimage"] != null)
                {
                    preImage = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                }


                //See if added by user (dc_addedbyuser)
                if (preImage != null && preImage.Contains("dc_foodnutrientid"))
                {
                    EntityReference foodNutrient = (EntityReference)preImage["dc_foodnutrientid"];
                    tracingService.Trace("foodNutrient.Id: " + foodNutrient.Id.ToString());
                    crmService.Delete("dc_food_nutrients", foodNutrient.Id);
                }
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }
    }
}
