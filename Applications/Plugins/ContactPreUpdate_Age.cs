﻿using System;
using Microsoft.Xrm.Sdk;


namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreCreate & PreUpdate message against the contact entity.  Calculates the dc_age attribute based on the value in the
    /// birthdate attribute
    /// </summary>
    public class ContactPreUpdate_Age : IPlugin
    {
        ITracingService tracingService = null;

        /// <summary>
        /// Calculate the age from the birthdate
        /// </summary>
        /// <param name="birthDate"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public int CalculateAgeFromBirthdate(DateTime birthDate, DateTime currentDate)
        {
            
            int ageValue = 0;
            try
            {
                int now = int.Parse(currentDate.ToString("yyyyMMdd"));
                int dob = int.Parse(birthDate.ToString("yyyyMMdd"));
                string dif = (now - dob).ToString();
                string age = "0";
                if (dif.Length > 4)
                {
                    age = dif.Substring(0, dif.Length - 4);
                }
                return (Convert.ToInt32(age));
                     
            }
            catch  (Exception ex)
            {
                tracingService.Trace("CalculateAgeFromBirthdate Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
                return (ageValue);
            }
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                // get the execution context from the service provider
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                tracingService.Trace("ContactPreUpdate_Age: starting: " + pluginExecutionContext.PrimaryEntityName);

                // get tge Entity object
                var contact = (Entity)pluginExecutionContext.InputParameters["Target"];

                if (contact.Attributes.Contains("birthdate"))
                {
                    var birthdate = (DateTime)contact["birthdate"];
                    var dateTimeCurrent = DateTime.Now;

                    tracingService.Trace("birthdate: " + birthdate);
                    tracingService.Trace("dateTimeCurrent: " + dateTimeCurrent);

                    // calculate the age 
                    int ageValue = CalculateAgeFromBirthdate(birthdate, dateTimeCurrent);

                    tracingService.Trace("ageValue: " + ageValue);

                    contact["dc_age"] = ageValue;
                }                                
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        } // end Execute

    }
}
