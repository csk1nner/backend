﻿using System;

using Microsoft.Xrm.Sdk;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreCreate & PreUpdate message against the contact entity.  Calculates the dc_dee attribute based 
    /// on the values in the following fields: dc_restinghours, 
    /// dc_verylighthours, dc_lighthours, dc_moderatehours, dc_heavyhours, dc_ree
    /// </summary>
    public class ContactPreUpdate_DEE : IPlugin
    {
        ITracingService tracingService = null;

        /// <summary>
        /// Calculate out the activity factor from  the provided hours of activity level
        /// </summary>
        /// <param name="restingHoursValue"></param>
        /// <param name="veryLightHoursValue"></param>
        /// <param name="lightHoursValue"></param>
        /// <param name="moderateHoursValue"></param>
        /// <param name="heavyHoursValue"></param>
        /// <returns></returns>
        public decimal CalculateActivityFactor(decimal restingHoursValue, 
            decimal veryLightHoursValue, 
            decimal lightHoursValue, 
            decimal moderateHoursValue,
            decimal heavyHoursValue)
        {
            var factorVeryLightHours = (decimal)1.5;
            var factorLightHours = (decimal)2.5;
            var factorModerateHours = (decimal)5.0;
            var factorHeavyHours = (decimal)7.0;
            var factor24 = (decimal)24;
            var activityFactorValue = (decimal)0.0; 
            try
            {
                activityFactorValue = (restingHoursValue + 
                    (factorVeryLightHours * veryLightHoursValue) + 
                    (factorLightHours * lightHoursValue) +
                    (factorModerateHours * moderateHoursValue) +
                    (factorHeavyHours * heavyHoursValue)) / factor24;                         
                return (activityFactorValue);
            }
            catch (Exception ex)
            {
                tracingService.Trace("CalculateActivityFactor Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
                return (activityFactorValue);
            }
        } // end CalculateActivityFactor

        public void Execute(IServiceProvider serviceProvider)
        {
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // get the execution context from the service provider
            IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            try
            {
                tracingService.Trace("ContactPreUpdate_DEE: starting: " + pluginExecutionContext.PrimaryEntityName+":"+pluginExecutionContext.MessageName);

                var restingHoursValue = (decimal)0.0;
                var veryLightHoursValue = (decimal)0.0;
                var lightHoursValue = (decimal)0.0;
                var moderateHoursValue = (decimal)0.0;
                var heavyHoursValue = (decimal)0.0;
                var REEValue = (decimal)0.0;

                // get the Entity contact
                var contact = (Entity)pluginExecutionContext.InputParameters["Target"];            

                // get the Entity preContact
                var preContact = new Entity();
                if (pluginExecutionContext.PreEntityImages.Contains("preimage"))
                {
                    preContact = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                }
                if (contact.Attributes.Contains("dc_restinghours") ||
                   contact.Attributes.Contains("dc_verylighthours") ||
                   contact.Attributes.Contains("dc_lighthours") ||
                   contact.Attributes.Contains("dc_moderatehours") ||
                   contact.Attributes.Contains("dc_heavyhours") ||
                   contact.Attributes.Contains("dc_ree"))                   
                {
                    // restingHoursValue
                    if (contact.Attributes.Contains("dc_restinghours"))
                    {
                        restingHoursValue = (decimal)contact["dc_restinghours"];                       
                    }
                    else if (preContact.Attributes.Contains("dc_restinghours"))
                    {
                        restingHoursValue = (decimal)preContact["dc_restinghours"];
                    }

                    tracingService.Trace("dc_restinghours: " + restingHoursValue);

                    // veryLightHoursValue
                    if (contact.Attributes.Contains("dc_verylighthours"))
                    {
                        veryLightHoursValue = (decimal)contact["dc_verylighthours"];                       
                    }
                    else if (preContact.Attributes.Contains("dc_verylighthours"))
                    {
                        veryLightHoursValue = (decimal)preContact["dc_verylighthours"];
                    }

                    tracingService.Trace("dc_verylighthours: " + veryLightHoursValue);

                    // lightHoursValue
                    if (contact.Attributes.Contains("dc_lighthours"))
                    {
                        lightHoursValue = (decimal)contact["dc_lighthours"];
                    }
                    else if (preContact.Attributes.Contains("dc_lighthours"))
                    {
                        lightHoursValue = (decimal)preContact["dc_lighthours"];
                    }

                    tracingService.Trace("dc_lighthours: " + lightHoursValue);

                    // moderateHoursValue
                    if (contact.Attributes.Contains("dc_moderatehours"))
                    {
                        moderateHoursValue = (decimal)contact["dc_moderatehours"];
                    }
                    else if (preContact.Attributes.Contains("dc_moderatehours"))
                    {
                        moderateHoursValue = (decimal)preContact["dc_moderatehours"];
                    }

                    tracingService.Trace("dc_moderatehours: " + moderateHoursValue);

                    // heavyHoursValue
                    if (contact.Attributes.Contains("dc_heavyhours"))
                    {
                        heavyHoursValue = (decimal)contact["dc_heavyhours"];
                    }
                    else if (preContact.Attributes.Contains("dc_heavyhours"))
                    {
                        heavyHoursValue = (decimal)preContact["dc_heavyhours"];
                    }

                    tracingService.Trace("dc_heavyhours: " + heavyHoursValue);

                    // REEValue - just in case
                    if (contact.Attributes.Contains("dc_ree"))
                    {
                        REEValue = (decimal)contact["dc_ree"];
                    }
                    else if (preContact.Attributes.Contains("dc_ree"))
                    {
                        REEValue = (decimal)preContact["dc_ree"];
                    }

                    tracingService.Trace("dc_restinghours: " + restingHoursValue);
                    tracingService.Trace("dc_verylighthours: " + veryLightHoursValue);
                    tracingService.Trace("dc_lighthours: " + lightHoursValue);
                    tracingService.Trace("dc_moderatehours: " + moderateHoursValue);
                    tracingService.Trace("dc_heavyhours: " + heavyHoursValue);
                    tracingService.Trace("dc_ree: " + REEValue);

                    // calculate the Activity Factor
                    var activityFactorValue = CalculateActivityFactor(restingHoursValue, 
                                                    veryLightHoursValue, 
                                                    lightHoursValue, 
                                                    moderateHoursValue, 
                                                    heavyHoursValue);

                    tracingService.Trace("Activity Factor: " + activityFactorValue);

                   // calculate the DEE value
                   var DEEValue = activityFactorValue * REEValue;

                   contact["dc_dee"] = DEEValue;
                   tracingService.Trace("dc_dee: " + DEEValue);
                }
                
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method: " + pluginExecutionContext.PrimaryEntityName + ":" + pluginExecutionContext.MessageName);
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        } // end Execute
    }
}
