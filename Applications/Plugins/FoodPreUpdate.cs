﻿using System;

using Microsoft.Xrm.Sdk;
using DynamicConnections.NutriStyle.CRM2011.Plugins.Helpers;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreUpdate message against the dc_foods entity.
    /// Only fires when the number of servings or the portion size of the food changes and the food is a recipe
    /// </summary>
    public class FoodPreUpdate : IPlugin
    {
        //private 
        
        private IOrganizationService crmService;


        private Entity food = null;

        bool isRecipe = false;

        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the organization service reference.
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);

            try
            {
                tracingService.Trace("----------------------------------------");
                tracingService.Trace("FoodPreUpdate: Starting: " + pluginExecutionContext.PrimaryEntityName);

                // get the Entity contact
                food = (Entity)pluginExecutionContext.InputParameters["Target"];

                // get the Entity preContact
                Entity preFood = new Entity();
                if (pluginExecutionContext.PreEntityImages.Count > 0)
                {
                    preFood = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                }

                tracingService.Trace("Found entities.  Starting gathering of variables");
                if (food.Contains("dc_numberofservings") || food.Contains("dc_portion_amount"))
                {
                    //Figure out unitGramWeight
                    double unitGramWeight = food.Contains("dc_unit_gram_weight") ? (double)food["dc_unit_gram_weight"] : 0d;
                    double portionSize = food.Contains("dc_portion_amount") ? (double)food["dc_portion_amount"] : 0d;
                    double numberOfServings = food.Contains("dc_numberofservings") ? (double)food["dc_numberofservings"] : 0d;
                    if (unitGramWeight == 0d)
                    {
                        unitGramWeight = preFood.Contains("dc_unit_gram_weight") ? (double)preFood["dc_unit_gram_weight"] : 0d;
                    }

                    if (food.Contains("dc_recipefood"))
                    {
                        isRecipe = (bool)food["dc_recipefood"];
                    }
                    else if (preFood.Contains("dc_recipefood"))
                    {
                        isRecipe = (bool)preFood["dc_recipefood"];
                    }

                    if (portionSize == 0d)
                    {
                        portionSize = preFood.Contains("dc_portion_amount") ? (double)preFood["dc_portion_amount"] : 0d;
                    }

                    if (numberOfServings == 0d)
                    {
                        numberOfServings = preFood.Contains("dc_numberofservings") ? (double)preFood["dc_numberofservings"] : 0d;
                    }
                    tracingService.Trace("isRecipe: " + isRecipe);
                    tracingService.Trace("numberOfServings: " + numberOfServings);
                    tracingService.Trace("portionSize: " + portionSize);

                    if (numberOfServings > 0d && isRecipe)
                    {
                        Ingredient i = new Ingredient();
                        unitGramWeight = i.Rollup(crmService, tracingService, food.Id, numberOfServings, portionSize, true);
                        tracingService.Trace("unitGramWeight: " + unitGramWeight);
                        //food["dc_unit_gram_weight"] = unitGramWeight;
                    }
                }
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }
        private decimal RetrieveMultipler(decimal mets) 
        {
            if (mets >= 1.6m && mets < 3)
            {
                return(.5m);
            }
            else if (mets >= 3m && mets < 6)
            {
                return(1m);
            }
            else if (mets >= 6m )
            {
                return(2m);
            }
            return (1);
        }
    }
}