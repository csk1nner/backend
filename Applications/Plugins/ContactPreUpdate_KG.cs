﻿using System;

using Microsoft.Xrm.Sdk;


namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreCreate & PreUpdate message against the contact entity.  Sets the dc_weightkg attribute based on
    /// what is in the dc_currentweight
    /// </summary>
    public class ContactPreUpdate_KG : IPlugin
    {
        //
        ITracingService tracingService = null;

        public decimal ConvertPoundToKG(int weightPound)
        {
            decimal corvertNumber = (decimal)2.2;
            decimal weightKG = (decimal)0.0;
            try
            {
                weightKG = (decimal)weightPound / corvertNumber;
                return (weightKG);
                }
            catch (Exception ex)
            {                
                tracingService.Trace("ConvertPoundToKG Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
                return (weightKG);
            }
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            // starts        
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
            // get the execution context from the service provider
            IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            tracingService.Trace("ContactPreUpdate_KG: starting: " + pluginExecutionContext.PrimaryEntityName);

                if (pluginExecutionContext.InputParameters.Contains("Target")) {
                    tracingService.Trace(" has target");
                }

                // get tge Entity object
                Entity contact = (Entity)pluginExecutionContext.InputParameters["Target"];

                // check for dc_currentweight field
                if (contact.Attributes.Contains("dc_currentweight"))
                {
                    // get the value of dc_currentweight in decimal
                    int currentwieght = (int)contact["dc_currentweight"];
                    tracingService.Trace("currentwieght: " + currentwieght);

                    // convert pound to KG
                    decimal wieghtKG = ConvertPoundToKG(currentwieght);
                    tracingService.Trace("wieghtKG: "+ wieghtKG);

                    // set the dc_weightkg field to wieghtKG
                    contact["dc_weightkg"] = wieghtKG;
                }
            }
            catch(Exception ex) 
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }            
        } // end Execute

    }
    
}

   