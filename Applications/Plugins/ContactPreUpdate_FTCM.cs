﻿using System;

using Microsoft.Xrm.Sdk;



namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreCreate & PreUpdate message against the contact entity.  Converts feet & inches to centimeters.  Looks 
    /// for dc_heightfeet or dc_heightinches attributes.  These two attributes are picklists (OptionSetValue)
    /// </summary>
    public class ContactPreUpdate_FTCM : IPlugin
    {
        ITracingService tracingService = null;

        public decimal ConvertFeetToCM(int valueFeet)
        {

            decimal corvertNumber = (decimal)30.48;
            decimal valueCM = (decimal)0.0;
            try
            {
                valueCM = (decimal)valueFeet * corvertNumber;
                return (valueCM);
            }
            catch (Exception ex)
            {
                tracingService.Trace("ConvertFootToCM Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
                return (valueCM);
            }
        }

        public decimal ConvertInchesToCM(int valueInches)
        {
            decimal corvertNumber = (decimal)2.54;
            decimal valueCM = (decimal)0.0;
            try
            {
                valueCM = (decimal)valueInches * corvertNumber;
                return (valueCM);
            }
            catch (Exception ex)
            {
                tracingService.Trace("ConvertInchToCM Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
                return (valueCM);
            }
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            var pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            try
            {
                tracingService.Trace("ContactPreUpdate_FTCM: starting: " + pluginExecutionContext.PrimaryEntityName+":"+pluginExecutionContext.MessageName);
                
                // initilize the variables 
                int heightFeet = 0;
                int heightInches = 0;

                int heightFeetValue = 0;
                int heightInchesValue = 0;
                
                //Form.  Data that the user changed
                Entity contact = (Entity)pluginExecutionContext.InputParameters["Target"];

                //RE Image.  Data before the user changed it
                Entity preContact = new Entity();
                if (pluginExecutionContext.PreEntityImages.Contains("preimage"))
                {
                    preContact = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                }

                if(contact.Attributes.Contains("dc_heightfeet") || contact.Attributes.Contains("dc_heightinches"))
                {
                    //Something has changed get values from form or preimage
                    if(contact.Attributes.Contains("dc_heightfeet")) 
                    {
                        heightFeetValue = ((OptionSetValue)contact["dc_heightfeet"]).Value;
                    }
                    else if (preContact.Attributes.Contains("dc_heightfeet")) 
                    {//get from pre image
                        heightFeetValue = ((OptionSetValue)preContact["dc_heightfeet"]).Value;
                    }

                    //Something has changed get values from form or preimage
                    if (contact.Attributes.Contains("dc_heightinches"))
                    {
                        heightInchesValue = ((OptionSetValue)contact["dc_heightinches"]).Value;
                    } 
                    else if (preContact.Attributes.Contains("dc_heightinches"))
                    {//get from pre image
                        heightInchesValue = ((OptionSetValue)preContact["dc_heightinches"]).Value;
                    }                    
                }

                tracingService.Trace("heightFeetValue: "+heightFeetValue);
                tracingService.Trace("heightInchesValue: " + heightInchesValue);

                if(heightInchesValue > 0 && heightFeetValue > 0) {
                    
                    //Feeds
                    if(heightFeetValue == 948170002) { //4 feet
                        heightFeet = 4;
                    }
                    else if (heightFeetValue == 948170000) 
                    { //5 feet
                        heightFeet = 5;
                    }
                    else if (heightFeetValue == 948170001)
                    { //6 feet
                        heightFeet = 6;
                    }
                    else if (heightFeetValue == 948170003)
                    { //7 feet
                        heightFeet = 7;
                    }
                    else if (heightFeetValue == 948170005)
                    { //2 feet
                        heightFeet = 2;
                    }
                    else if (heightFeetValue == 948170004)
                    { //3 feet
                        heightFeet = 3;

                    }

                    //Inches
                    if(heightInchesValue == 948170000) { 
                    // 1 n
                        heightInches = 1;
                    }
                    else if (heightInchesValue == 948170001) 
                    { // 2 n
                        heightInches = 2;
                    }
                    else if (heightInchesValue == 948170002) 
                    { // 3 n
                        heightInches = 3;
                    }
                    else if (heightInchesValue == 948170003)
                    { // 4 n
                        heightInches = 4;
                    }
                    else if (heightInchesValue == 948170004)
                    { // 5 n
                        heightInches = 5;
                    }
                    else if (heightInchesValue == 948170005)
                    { // 6 n
                        heightInches = 6;
                    }
                    else if (heightInchesValue == 948170006)
                    { // 7 n
                        heightInches = 7;
                    }
                    else if (heightInchesValue == 948170007)
                    { // 8 n
                        heightInches = 8;
                    }
                    else if (heightInchesValue == 948170008)
                    { // 9 n
                        heightInches = 9;
                    }
                    else if (heightInchesValue == 948170009)
                    { // 10 n
                        heightInches = 10;
                    }
                    else if (heightInchesValue == 948170010)
                    { // 11 n
                        heightInches = 11;
                    }
                    else if (heightInchesValue == 948170011)
                    { // 0 n
                        heightInches = 0;
                    }
                }

                tracingService.Trace("heightFeet: " + heightFeet);
                tracingService.Trace("heightInches: " + heightInches);

                if (heightFeet > 0 && heightInches >= 0)
                {
                   // get the feet in CM
                   decimal valueFeetToCM = ConvertFeetToCM(heightFeet);
                   tracingService.Trace("FeetToCM: " + valueFeetToCM);

                   // calculate  the inches in CM
                   decimal valueInchesToCM = ConvertInchesToCM(heightInches);
                   tracingService.Trace("InchesToCM: " + valueInchesToCM);

                   // get total CM
                   decimal totalCM = valueFeetToCM + valueInchesToCM;

                   // set the field value dc_heightcm to totalCM
                   contact["dc_heightcm"] = totalCM;
                   tracingService.Trace("Total Feet + Inches: " + totalCM);
                }
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method: " + pluginExecutionContext.PrimaryEntityName + ":" + pluginExecutionContext.MessageName);
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }

        }
    }
}
