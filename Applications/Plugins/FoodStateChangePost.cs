﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk.Messages;

using Microsoft.Xrm.Sdk.Query;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the SetState & SetStateDynamicEntity message against the dc_foods entity.  Updates the status of
    /// the related dc_food_nutrients entity to match the state of the dc_foods.
    /// </summary>
    public class FoodStateChangePost : IPlugin
    {
        //
        
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // get the execution context from the service provider temp code to makea  change
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);
                tracingService.Trace("Preset: starting: " + pluginExecutionContext.PrimaryEntityName + ": " + pluginExecutionContext.MessageName);
                EntityReference target = (EntityReference)pluginExecutionContext.InputParameters["EntityMoniker"];
                Entity postImage = crmService.Retrieve("dc_foods", target.Id, new ColumnSet(new String[] { "dc_name", "dc_foodnutrientid" }));

                if (postImage.Contains("dc_foodnutrientid") && pluginExecutionContext.InputParameters.Contains("State"))
                {
                    
                    tracingService.Trace("Test: State = " + pluginExecutionContext.InputParameters["State"]);

                    Guid nutrientId = ((EntityReference)postImage["dc_foodnutrientid"]).Id;
                    Entity nutrient = crmService.Retrieve("dc_food_nutrients", nutrientId, new ColumnSet(new String[] { "dc_name" }));

                    tracingService.Trace("Retrieved: Nutrient = " + nutrient.LogicalName + ". Updating state to match the food");

                    SetStateRequest nutrientReq = new SetStateRequest();
                    nutrientReq.EntityMoniker = new EntityReference(nutrient.LogicalName, nutrientId);
                    nutrientReq.State = (OptionSetValue)pluginExecutionContext.InputParameters["State"];
                    nutrientReq.Status = new OptionSetValue(-1);
                    tracingService.Trace("Set: Nutrient State: " + nutrientReq.State + ". Nutrient Status: " + nutrientReq.Status);
                    crmService.Execute(nutrientReq);
                    tracingService.Trace("Result: State Changed, the nutrient State has been updated.");
                }
                else
                {
                    tracingService.Trace("Result: Food does not contain a nutrient");
                }

            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }
    }
}
