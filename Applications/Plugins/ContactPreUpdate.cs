﻿using System;
using System.Collections.Generic;

using Microsoft.Xrm.Sdk;


namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreUpdate & PreCreate message against the contact entity.  Sets the activity levels based off of a preset selection (dc_activitylevel) by the user.
    /// Sets the following attributes: dc_restinghours, dc_verylighthours, dc_lighthours, dc_moderatehours, dc_heavyhours.  
    /// 
    /// </summary>
    public class ContactPreUpdate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                // get the execution context from the service provider
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                tracingService.Trace("ContactPreUpdate: starting: " + pluginExecutionContext.PrimaryEntityName);

                // initilize the variables                
                int activitylevelValue = 0;
                string activitylevelName= string.Empty;

                // form - data that the user changed
                Entity contact = (Entity)pluginExecutionContext.InputParameters["Target"];

                // get the dc_activitylevel values from OptionSetValue
                if (contact.Attributes.Contains("dc_activitylevel"))
                {
                    activitylevelValue = ((OptionSetValue)contact["dc_activitylevel"]).Value;
                }

                tracingService.Trace("activitylevelValue: " + activitylevelValue);
                tracingService.Trace("activitylevelName: " + activitylevelName);

                // get the activitylevelName as s, la, ma and ha
                if (activitylevelValue > 0)
                {
                    if (activitylevelValue == 948170000)
                    {
                        activitylevelName = "s";
                    }
                    else if (activitylevelValue == 948170001)
                    {
                        activitylevelName = "la";
                    }
                    else if (activitylevelValue == 948170002)
                    {
                        activitylevelName = "ma";
                    }
                    else if (activitylevelValue == 948170003)
                    {
                        activitylevelName = "ha";
                    }
                }

                tracingService.Trace("activitylevelValue: " + activitylevelValue);
                tracingService.Trace("activitylevelName: " + activitylevelName);
                
                // all fields must be in contact Entity
                if (activitylevelName != string.Empty)
                {                   
                    // for activitylevelName = s
                    if (activitylevelName.Equals("s", StringComparison.OrdinalIgnoreCase))
                    {
                        contact["dc_restinghours"] = (decimal)12.0;
                        contact["dc_verylighthours"] = (decimal)12.0;
                        contact["dc_lighthours"] = (decimal)0.0;
                        contact["dc_moderatehours"] = (decimal)0.0;
                        contact["dc_heavyhours"] = (decimal)0.0;
                    }
                    // for activitylevelName = la
                    if (activitylevelName.Equals("la", StringComparison.OrdinalIgnoreCase))
                    {
                        contact["dc_restinghours"] = (decimal)8.0;
                        contact["dc_verylighthours"] = (decimal)12.0;
                        contact["dc_lighthours"] = (decimal)3.0;
                        contact["dc_moderatehours"] = (decimal)1.0;
                        contact["dc_heavyhours"] = (decimal)0.0;                            
                    }
                    // for activitylevelName = ma
                    if (activitylevelName.Equals("ma", StringComparison.OrdinalIgnoreCase))
                    {
                        contact["dc_restinghours"] = (decimal)8.0;
                        contact["dc_verylighthours"] = (decimal)9.0;
                        contact["dc_lighthours"] = (decimal)4.0;
                        contact["dc_moderatehours"] = (decimal)2.0;
                        contact["dc_heavyhours"] = (decimal)1.0;
                    }
                    // for activitylevelName = ha
                    if (activitylevelName.Equals("ha", StringComparison.OrdinalIgnoreCase))
                    {
                        contact["dc_restinghours"] = (decimal)8.0;
                        contact["dc_verylighthours"] = (decimal)5.0;
                        contact["dc_lighthours"] = (decimal)4.0;
                        contact["dc_moderatehours"] = (decimal)2.0;
                        contact["dc_heavyhours"] = (decimal)5.0;
                    }                                
                }
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }

        }



    }
}
