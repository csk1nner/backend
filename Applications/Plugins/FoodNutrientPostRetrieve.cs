﻿using System;
using System.Collections.Generic;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Query;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PostRetrieve message against the dc_food_nutrients.  
    /// Plugin pulls values to populate fields when looking at a specific food.
     
    /// </summary>
    public class FoodNutrientPostRetrieve : IPlugin
    {
       
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // get the execution context from the service provider
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);

                String name = String.Empty;
                EntityReference portionTypeId = null;
                double portionAmount = 0d;
                double unitGramWeight = 0d;


                tracingService.Trace("FoodNutrientPostRetrieve: starting: " + pluginExecutionContext.PrimaryEntityName);
                //tracingService.Trace("test");
                //Find the guid that draws the relationship back to the dc_food entity (dc_foodnutrientid)
                Entity foodNutrient = (Entity)pluginExecutionContext.OutputParameters["BusinessEntity"];
                Guid dc_foodnutrientid = Guid.Empty;
                /*
                if (foodNutrient.Contains("dc_foodnutrientid"))
                {
                    dc_foodnutrientid = foodNutrient.Attributes["dc_foodnutrientid"] == null ? Guid.Empty : (Guid)foodNutrient.Attributes["dc_foodnutrientid"];
                }
                */

                //DataCollection<Entity> list = CrmHelper.GetEntitiesByAttribute("dc_foods", "dc_foodnutrientid", foodNutrient.Id, new ColumnSet(new String[] { "dc_name", "dc_portiontypeid", "dc_portion_amount", "dc_unit_gram_weight" }), crmService);//need to add other three attributes
                //DataCollection<Entity> list = null;
                //Get the dc_food for the dc_food_nutrients (dc_foodnutrientid)
                
                //"dc_name", "dc_portiontypeid", "dc_portion_amount", "dc_unit_gram_weight"


                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_foods'>
                        <attribute name='dc_name' />
                        <attribute name='dc_portiontypeid' />
                        <attribute name='dc_portion_amount' />
                        <attribute name='dc_unit_gram_weight' />

                        <filter type='and'>
                          <condition attribute='dc_foodnutrientid' operator='eq' value='@DC_FOODNUTRIENTID' />
                        </filter>
                       
                      </entity>
                    </fetch>";

                fetchXml = fetchXml.Replace("@DC_FOODNUTRIENTID", foodNutrient.Id.ToString());

                EntityCollection list = null;
                try
                {
                    list = crmService.RetrieveMultiple(new FetchExpression(fetchXml));
                }
                catch (Exception ex)
                {
                    tracingService.Trace("fetchxml retrieve");
                    tracingService.Trace(ex.Message);
                    tracingService.Trace(ex.StackTrace);
                }

                if (list != null && list.Entities.Count > 0)
                {
                    Entity food = list[0];
                    if (food.Contains("dc_name"))
                    {
                        name = food.Attributes["dc_name"] == null ? String.Empty : (String)food.Attributes["dc_name"];
                    }
                    else
                    {
                        tracingService.Trace("Not able to find related food (dc_foods)");
                    }
                    if (food.Contains("dc_portiontypeid"))
                    {
                        //tracingService.Trace("type:" + food.Attributes["dc_portiontypeid"].GetType());
                        portionTypeId = food.Attributes["dc_portiontypeid"] == null ? null : (EntityReference)food.Attributes["dc_portiontypeid"];
                    }
                    else
                    {
                        tracingService.Trace("Not able to find related food (dc_portiontypeid)");
                    }
                    if (food.Contains("dc_portion_amount"))
                    {
                        //tracingService.Trace("type:" + food.Attributes["dc_portion_amount"].GetType());
                        portionAmount = food.Attributes["dc_portion_amount"] == null ? 0.0 : (double)food.Attributes["dc_portion_amount"];
                    }
                    else
                    {
                        //tracingService.Trace("type:" + food.Attributes["dc_portiontypeid"].GetType());
                        tracingService.Trace("Not able to find related food (dc_portiontypeid)");
                    }
                    if (food.Contains("dc_unit_gram_weight"))
                    {
                        unitGramWeight = food.Attributes["dc_unit_gram_weight"] == null ? 0.0 : (double)food.Attributes["dc_unit_gram_weight"];
                    }
                    else
                    {
                        tracingService.Trace("Not able to find related food (dc_unit_gram_weight)");
                    }
                }
              
                //update retreived entity
                if (!String.IsNullOrEmpty(name))
                {
                    foodNutrient["dc_name"] = name;
                }
                if (portionTypeId != null)
                {
                    foodNutrient["dc_portiontypeid"] = portionTypeId;
                }
                if (portionAmount != 0d)
                {
                    foodNutrient["dc_portion_amount"] = portionAmount;
                }
                if (unitGramWeight != 0d)
                {
                    foodNutrient["dc_unit_gram_weight"] = unitGramWeight;
                }

            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }

        }



    }
}
