﻿using System;
using Microsoft.Xrm.Sdk;
//
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Messages;

namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Registered against the PreUpdate message against the dc_food entity.  This is part of the clone food feature that is 
    /// kicked off in the UI (Dynamics) by a javascript event firing when a button is clicked on.  Plugin can also be fired by 
    /// setting the dc_clone_food attribute to true.  Does not look to be registered!
    /// </summary>
    public class CloneFoodPreUpdate : IPlugin
    {
       
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);

                tracingService.Trace("CloneFoodPreUpdate: starting: " + pluginExecutionContext.PrimaryEntityName + ": " + pluginExecutionContext.MessageName);

                var preImage = (Entity)pluginExecutionContext.PreEntityImages["preimage"];
                var target = (Entity)pluginExecutionContext.InputParameters["Target"];
                
                if (target.Contains("dc_clone_food"))
                {
                    //tracingService.Trace("Check: value of dc_clone_food: " + target["dc_clone_food"]);

                    if ((Boolean)target["dc_clone_food"])
                    {
                        target["dc_clone_food"] = false;

                        Guid newId = Guid.NewGuid();
                        tracingService.Trace("Condition: dc_clone_food is checked. Cloning the food: " + preImage["dc_name"]);
                        //Need to create a new entity.
                        Entity cloneFood = crmService.Retrieve("dc_foods", preImage.Id, new ColumnSet(true));//This gets all the relationships
                        
                        cloneFood["dc_foodsid"] = newId;
                        cloneFood.Id = newId;
                        preImage["dc_clone_food"] = false;
                        
                        Boolean cloneNutrient = false;

                        cloneFood["dc_name"] = "Copy " + cloneFood["dc_name"];
                        Guid nutrientId = new Guid();

                        if (cloneFood.Contains("dc_foodnutrientid"))
                        {
                            tracingService.Trace("Condition: Contains a dc_foodnutrientid. Store guid and set clone to true");
                            // Store the Guid of the nutrient if it exists
                            nutrientId = ((EntityReference)cloneFood["dc_foodnutrientid"]).Id;
                            cloneNutrient = true;
                            tracingService.Trace("Condition: finished Guid copy and cloneNutrient to true");
                            cloneFood["dc_foodnutrientid"] = newId;
                        }

                        if (cloneNutrient)
                        {
                            // clone the nutrient
                            tracingService.Trace("Condition: Nutrient for the food exists, Cloning nutrient");

                            Entity nutrient = crmService.Retrieve("dc_food_nutrients", nutrientId, new ColumnSet(true));

                            nutrient["dc_food_nutrientsid"] = newId;
                            nutrient.Id = newId;
                            nutrient["dc_name"] = "Copy - " + nutrient["dc_name"];
                            //create the nutrient and set the food dc_foodnutrientid to the new guid
                            Guid nId = crmService.Create(nutrient);
                            cloneFood["dc_foodnutrientid"] = new EntityReference("dc_food_nutrients", nId);
                            tracingService.Trace("Success: Nutrient created");
                        }
                        else
                        {
                            tracingService.Trace("No nutrient information on the food");
                        }

                        //create the Food
                        tracingService.Trace("Creating food");
                        Guid food = crmService.Create(cloneFood);
                        tracingService.Trace("Complete: Clone has executed successfully: "+food+":"+cloneFood.Id);
                        //Deal with ingredients
                        String fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='dc_ingredient'>
                            <all-attributes />
                            <filter type='and'>
                              <condition attribute='dc_foodid' operator='eq' value='@FOODID' />
                            </filter>
                          </entity>
                        </fetch>";

                        fetchXml = fetchXml.Replace("@FOODID", preImage.Id.ToString());

                        EntityCollection collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));

                        if (collection != null && collection.Entities.Count > 0)
                        {
                            foreach (Entity entity in collection.Entities)
                            {
                                entity.Id = Guid.NewGuid();
                                entity["dc_ingredientid"] = entity.Id;
                                entity["dc_foodid"] = new EntityReference("dc_foods", food);
                                crmService.Create(entity);
                                tracingService.Trace("Created ingredient: " + entity.Id.ToString());

                            }
                        }
                        tracingService.Trace("Done with ingredients");
                        //deal with MtM relationship
                        fetchXml = "<fetch mapping='logical' no-lock='true' > <entity name='dc_dc_foods_dc_meal_component'>"
                            + "<all-attributes />"
                            + "<filter>"
                            + "<condition attribute='dc_foodsid' operator='eq' value ='" + preImage.Id.ToString() + "' />"
                            + "</filter>"
                            + "</entity>"
                            + "</fetch>";

                        // Perform the query
                        collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (collection != null && collection.Entities.Count > 0)
                        {
                            tracingService.Trace("Found " + collection.Entities.Count + " MtM relationships that need cloned");
                            foreach (Entity entity in collection.Entities)
                            {
                                if(entity.Contains("dc_meal_componentid")) {
                                    Guid subCategoryId = (Guid)entity["dc_meal_componentid"];
                                    tracingService.Trace("subCategoryId: " + subCategoryId);
                                    tracingService.Trace("cloneFood.Id: " + cloneFood.Id);
                                    //Create relationship
                                    AssociateRequest request = new AssociateRequest()
                                    {
                                        RequestId = Guid.NewGuid(),
                                        Target = new EntityReference("dc_foods", cloneFood.Id),
                                        RelatedEntities = new EntityReferenceCollection
                                        {
                                            new EntityReference("dc_meal_component", subCategoryId)
                                        },
                                        Relationship = new Microsoft.Xrm.Sdk.Relationship("dc_dc_foods_dc_meal_component")
                                    };
                                    tracingService.Trace("Creating relationship");
                                    //crmService.Execute(request);
                                    tracingService.Trace("Created relationship");
                                }
                            }
                        }
                    }
                    else
                    {
                        tracingService.Trace("Condition: dc_clone_food is not checked, not cloning the food");
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("Code: " + ex.Detail.ErrorCode);
                tracingService.Trace("Message: " + ex.Detail.Message);
                tracingService.Trace("Trace: " + ex.Detail.TraceText);
                tracingService.Trace("Inner Fault: " + ex.Detail.InnerFault);
            }


            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
        }
        private static bool DoesRelationshipExist(IOrganizationService crmService, string relationshipSchemaName,
            string entity1SchemaName, Guid entity1KeyValue, string entity2SchemaName, Guid entity2KeyValue)
        {
            // Assemble FetchXML to query the intersection entity directly
            string fetchXml = "<fetch mapping='logical' no-lock='true' > <entity name='" + relationshipSchemaName + "'>"
            + "<all-attributes />"
            + "<filter>"
            + "<condition attribute='" + entity1SchemaName + "id' operator='eq' value ='" + entity1KeyValue.ToString() + "' />"
            + "<condition attribute='" + entity2SchemaName + "id' operator='eq' value='" + entity2KeyValue.ToString() + "' />"
            + "</filter>"
            + "</entity>"
            + "</fetch>";

            // Perform the query
            var collection = crmService.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collection.Entities.Count == 0)
                return false;
            else
                return true;
        }
    }
}
