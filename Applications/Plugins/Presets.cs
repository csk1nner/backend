﻿using System;
using System.Collections.Generic;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Query;


namespace DynamicConnections.NutriStyle.CRM2011.Plugins
{
    /// <summary>
    /// Plugin pulls values from presets.  If the total percentage is not 100%, calculate the difference.
    /// Not registered at this time.
    /// 
    /// </summary>
    public class Presets : IPlugin
    {
      
        public void Execute(IServiceProvider serviceProvider)
        {
            var tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            try
            {
                // get the execution context from the service provider
                IPluginExecutionContext pluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService crmService = serviceFactory.CreateOrganizationService(pluginExecutionContext.UserId);

                tracingService.Trace("Preset: starting: " + pluginExecutionContext.PrimaryEntityName + ":" + pluginExecutionContext.MessageName);

                Entity presets;

                double breakfast = 0d;
                double lunch = 0d;
                double dinner = 0d;
                double fat = 0d;
                double cho = 0d;
                double pro = 0d;
                double missingPercent = 0d;
               
                if (pluginExecutionContext.InputParameters.Contains("Target") && pluginExecutionContext.InputParameters["Target"] is Entity)
                {

                    // Obtain the target business entity from the input parmameters.
                    presets = (Entity)pluginExecutionContext.InputParameters["Target"];

                }
                else
                {
                    return;
                }

                if (presets.Attributes.Contains("dc_breakfast_percent") && presets.Attributes.Contains("dc_lunch_percent") && presets.Attributes.Contains("dc_dinner_percent"))
                {
                    tracingService.Trace("All three meals contain percentages");
                }
                else
                {

                    if (presets.Attributes.Contains("dc_breakfast_percent"))
                    {
                        //tracingService.Trace("breakfast percent: " + presets.Attributes["dc_breakfast_percent"].GetType());
                        breakfast = Convert.ToDouble((decimal)presets.Attributes["dc_breakfast_percent"]);
                    }

                    else
                    {
                        tracingService.Trace("No breakfast percentage");


                    }
                    if (presets.Attributes.Contains("dc_lunch_percent"))
                    {
                        //tracingService.Trace("lunch percent: " + presets.Attributes["dc_lunch_percent"].GetType());
                        lunch =  Convert.ToDouble((decimal)presets.Attributes["dc_lunch_percent"]);
                    }

                    else
                    {
                        tracingService.Trace("No lunch percentage");

                    }
                    if (presets.Attributes.Contains("dc_dinner_percent"))
                    {
                        //tracingService.Trace("dinner percent: " + presets.Attributes["dc_dinner_percent"].GetType());
                        dinner =  Convert.ToDouble((decimal)presets.Attributes["dc_dinner_percent"]);
                    }

                    else
                    {
                        tracingService.Trace("No dinner percentage");

                    }
                    missingPercent = 100 - (breakfast + lunch + dinner);
                    tracingService.Trace("missing percent: " + missingPercent);

                    if(breakfast != 0 && lunch != 0)
                    {
                        presets.Attributes["dc_dinner_percent"] = missingPercent;   
                        //tracingService.Trace("dinner percent: " + presets.Attributes["dc_dinner_percent"].ToString());
                    }
                    else if(lunch != 0 && dinner != 0)
                    {
                        presets.Attributes["dc_breakfast_percent"] = missingPercent;
                        //tracingService.Trace("breakfast percent: " + presets.Attributes["dc_breakfast_percent"].ToString());
                    }
                    else if(breakfast != 0 && dinner != 0)
                    {
                        presets.Attributes["dc_lunch_percent"] = missingPercent;
                        //tracingService.Trace("lunch percent: " + presets.Attributes["dc_lunch_percent"].ToString());
                    }
                }


                if (presets.Attributes.Contains("dc_cho_pct") && presets.Attributes.Contains("dc_pro_pct") && presets.Attributes.Contains("dc_fat_pct"))
                {
                    tracingService.Trace("All three nutrients contain percentages");
                }
                else
                {

                    if (presets.Attributes.Contains("dc_cho_pct"))
                    {
                        //tracingService.Trace("cholesterol percent: " + presets.Attributes["dc_cho_pct"].GetType());
                        cho = Convert.ToDouble((int)presets.Attributes["dc_cho_pct"]);
                    }

                    else
                    {
                        tracingService.Trace("No cholesterol percentage");


                    }
                    if (presets.Attributes.Contains("dc_pro_pct"))
                    {
                        //tracingService.Trace("protein percent: " + presets.Attributes["dc_pro_pct"].GetType());
                        pro = Convert.ToDouble((int)presets.Attributes["dc_pro_pct"]);
                    }

                    else
                    {
                        tracingService.Trace("No protein percentage");

                    }
                    if (presets.Attributes.Contains("dc_fat_pct"))
                    {
                        //tracingService.Trace("fat percent: " + presets.Attributes["dc_fat_pct"].GetType());
                        fat = Convert.ToDouble((int)presets.Attributes["dc_fat_pct"]);
                    }

                    else
                    {
                        tracingService.Trace("No fat percentage");

                    }
                    missingPercent = 100 - (pro + cho + fat);
                    //tracingService.Trace("missing percent: " + missingPercent);

                    if (pro != 0 && cho != 0)
                    {
                        presets.Attributes["dc_fat_pct"] = missingPercent;
                        //tracingService.Trace("fat percent: " + presets.Attributes["dc_fat_pct"].ToString());
                    }
                    else if (cho != 0 && fat != 0)
                    {
                        presets.Attributes["dc_pro_pct"] = missingPercent;
                        //tracingService.Trace("protein percent: " + presets.Attributes["dc_pro_pct"].ToString());
                    }
                    else if (fat != 0 && pro != 0)
                    {
                        presets.Attributes["dc_cho_pct"] = missingPercent;
                        //tracingService.Trace("cholesterol percent: " + presets.Attributes["dc_cho_pct"].ToString());
                    }
                }
      
            }
            catch (Exception ex)
            {
                tracingService.Trace("Execute Method");
                tracingService.Trace(ex.Message);
                tracingService.Trace(ex.StackTrace);
            }
            
        }
    }
}
