﻿
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public abstract class LogHelperBase
    {

        public LogHelperBase()
        { }

        //public abstract List<LogViewModel> Retrieve(int offset, int count);

        public void CreateLog(string source)
        {
            CreateLog(source, String.Empty, String.Empty);
        }

        public void CreateLog(string source, string message)
        {
            CreateLog(source, message, String.Empty);
        }
        public void CreateLog(String source, int message)
        {
            CreateLog(source, message.ToString());
        }
        public void CreateLog(String source, Guid message)
        {
            CreateLog(source, message.ToString());
        }


        public void CreateLog(string source, Exception e)
        {
            ///if (Globals.IsLogEnabled)
            {
                var l = new Log()
                {
                    Source = source,
                };
                if (e != null)
                {
                    l.Message = e.Message ;
                    l.StackTrace = e.StackTrace;
                }

                try
                {
                    Create(l);
                }
                catch (Exception)
                {
                }
            }
        }

        public void CreateLog(string source, string message, string stacktrace)
        {
           // if (Globals.IsLogEnabled)
            {
                //If debug write to output

                Debug.WriteLine(source + " - " + message + " - " + stacktrace);

                var l = new Log()
                {
                    Source = source,
                    Message = message ,
                    StackTrace = stacktrace
                };
                try
                {
                    Create(l);
                }
                catch (Exception)
                {
                }
            }
        }

        public abstract void Create(Log l);

        public abstract void Cleanup();

    }
}
