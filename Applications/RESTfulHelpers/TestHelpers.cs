﻿using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public class TestHelpers
    {

        public User CreateUser()
        {
            try
            {
                var uh = new UserHelper();

                uh.CreateUser("test@test.com", "1234123412341234",  "12345", "testsmith", "testsmith", 
                    new Guid("6B3C82E0-F209-E111-9777-00155D0A0205"), 
                    new Guid("23ECAC37-490A-E111-9777-00155D0A0205"),
                    new Guid("875A6564-490A-E111-9777-00155D0A0205"),
                    new Guid("79AA100A-3CA5-E111-8777-00155D0A0C06"),
                    "gtest gg",
                    new Guid("3FE43C94-54D0-E111-8B24-00155D0A0C06")
                    );

            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("TestHelpers.cs: CreateUser()", err);
            }
            return(null);
        }
    }
}
