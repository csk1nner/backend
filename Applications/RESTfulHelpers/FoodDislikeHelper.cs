﻿using DynamicConnections.Nutristyle.Dynamics8Helpers;
using Newtonsoft.Json.Linq;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public class FoodDislikeHelper
    {
        /// <summary>
        /// Retrieve the foodlikes for the user in question
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<FoodDislike> RetrieveFoodDislikes(Guid userId)
        {
            try
            {

                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                  <entity name='dc_fooddislike'>
                    <attribute name='dc_foodid' />
                    <attribute name='dc_mealcomponentid' />
                    <attribute name='dc_fooddislikeid' />
                    <order attribute='dc_foodid' descending='false' />
                   
                    <link-entity name='dc_foods' from='dc_foodsid' to='dc_foodid' alias='dc_foods' link-type='outer'>
                        <attribute name='dc_name' />
                    </link-entity>
                    <link-entity name='dc_meal_component' from='dc_meal_componentid' to='dc_mealcomponentid' alias='dc_mealcomponent' link-type='outer'>
                        <attribute name='dc_name' />
                    </link-entity>

                    <filter type='and'>
                        <condition attribute='dc_contactid' value='@CONTACTID' operator='eq'/>
                    </filter>
                  </entity>
                </fetch>";

                fetchXml = fetchXml.Replace("@CONTACTID", userId.ToString());

                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.FoodDislike, fetchXml).Result;
                var cf = new ConvertFrom();
                var foodDisikes = (List<FoodDislike>)cf.Execute<List<FoodLike>>(DynamicsConnection.RegardingTypeCode.FoodDislike, results);
                
                return (foodDisikes);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodDislikeHelper.cs: RetrieveFoodDislikes()", err);
            }

            return (null);
        }
        
        /// <summary>
        /// Creates a foodislike.
        /// </summary>
        /// <param name="fl"></param>
        /// <returns></returns>
        public Guid CreateFoodDislike(FoodDislike fd)
        {
            try
            {
                var entity = BuildJObject(fd);

                var dc = new DynamicsConnection();
                var Id = dc.Create(DynamicsConnection.RegardingTypeCode.FoodDislike, entity).Result;
                return (Id);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: CreateFoodDislike(FoodDislike)", err);
            }
            return (Guid.Empty);
        }
        
        public bool UpdateFoodDislike(FoodDislike fd)
        {
            try
            {
                var entity = BuildJObject(fd);
                var dc = new DynamicsConnection();
                var result = dc.Update(DynamicsConnection.RegardingTypeCode.FoodLike, fd.FoodDislikeId, entity).Result;
                return (result);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: UpdateFoodDislike(FoodDislike)", err);
            }

            return (false);
        }

        public bool DeleteFoodDislike(Guid foodDislikeId)
        {
            try
            {
                var dc = new DynamicsConnection();
                var result = dc.Delete(DynamicsConnection.RegardingTypeCode.FoodDislike, foodDislikeId).Result;
                return (result);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: DeleteFoodLike(Guid)", err);
            }

            return (false);
        }

        private JObject BuildJObject(FoodDislike fd)
        {
            var entity = new JObject
                {
                   
                };
            if (fd.UserId != Guid.Empty)
            {
                entity.Add("dc_contactid@odata.bind", "/contacts(" + fd.UserId + ")");
            }
            if (fd.FoodId != Guid.Empty)
            {
                entity.Add("dc_foodid@odata.bind", "/dc_foodses(" + fd.FoodId + ")");
            }
            if (fd.MealComponentId != Guid.Empty)
            {
                entity.Add("dc_mealcomponentid@odata.bind", "/dc_meal_components(" + fd.MealComponentId + ")");
            }

            return (entity);
        }

    }
}
