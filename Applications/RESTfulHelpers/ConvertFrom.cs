﻿using DynamicConnections.Nutristyle.Dynamics8Helpers;

using Newtonsoft.Json.Linq;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public class ConvertFrom
    {




        public int Aggregate(JObject jObject, String name)
        {

            try
            {
                //var count = RetrieveValue<int>(jObject, name);
                var count = (int)jObject["value"][0][name];
                return (count);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: Aggregate(JObject, String)", err);
            }
            return (-1);
            //entity["value"][0]["emailcount"]
        }

        /// <summary>
        /// Convert from the strongly typed entity to a JObject for passing to  Dynamics
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="regardingObjectType"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public JObject ConvertTo<T>(DynamicsConnection.RegardingTypeCode regardingObjectType, T entity)
        {

            try
            {
                var jEntity = new JObject
                    {
                       
                    };
                if (entity is User)
                {
                    var u = (User)Convert.ChangeType(entity, typeof(T), new CultureInfo("en-US"));

                    //jEntity.Add("contactid", u.UserId);
                    //Strings
                    if (!String.IsNullOrEmpty(u.FirstName))
                    {
                        jEntity.Add("firstname", u.FirstName);
                    }
                    if (!String.IsNullOrEmpty(u.LastName))
                    {
                        jEntity.Add("lastname", u.LastName);
                    }
                    if (!String.IsNullOrEmpty(u.EmailAddress1))
                    {
                        jEntity.Add("emailaddress1", u.EmailAddress1);
                    }
                    //Datetimes
                    if (u.Birthdate != DateTime.MinValue)
                    {
                        jEntity.Add("birthdate", u.Birthdate);
                    }

                    //ints and picklists
                    if (u.GenderCode > 0)
                    {
                        jEntity.Add("gendercode", u.GenderCode);
                    }
                    if (u.CurrentWeight > 0)
                    {
                        jEntity.Add("dc_currentweight", u.CurrentWeight);
                    }
                    if (u.HeightFeet > 0)
                    {
                        jEntity.Add("dc_heightfeet", u.HeightFeet);
                    }
                    if (u.HeightInches > 0)
                    {
                        jEntity.Add("dc_heightinches", u.HeightInches);
                    }
                   
                    if (u.ActivityLevel > 0)
                    {
                        jEntity.Add("dc_activitylevel", u.ActivityLevel);
                    }
                    if (u.PoundsPerWeek > 0)
                    {
                        jEntity.Add("dc_poundsperweek", u.PoundsPerWeek);
                    }
                    if (u.KCalCalculatedtarget > 0)
                    {
                        jEntity.Add("dc_kcalcalculatedtarget", u.KCalCalculatedtarget);
                    }
                    if (u.WeightKG > 0)
                    {
                        jEntity.Add("dc_weightkg", u.WeightKG);
                    }
                    if (u.HeightCM > 0)
                    {
                        jEntity.Add("dc_heightcm", u.HeightCM);
                    }
                    if (u.Age > 0)
                    {
                        jEntity.Add("dc_age", u.Age);
                    }
                    if (u.BMI > 0)
                    {
                        jEntity.Add("dc_bmi", u.BMI);
                    }
                    if (u.DEE > 0)
                    {
                        jEntity.Add("dc_dee", u.DEE);
                    }
                    if (u.TargetWeight > 0)
                    {
                        jEntity.Add("dc_targetweight", u.TargetWeight);
                    }

                    //bools
                    if (u.MaintainTargetWeight != null)
                    {
                        jEntity.Add("dc_maintaintargetweight", u.MaintainTargetWeight.Value);
                    }

                    if (u.MorningSnack != null)
                    {
                        jEntity.Add("dc_morningsnack", u.MorningSnack);
                    }
                    if (u.AfternoonSnack != null)
                    {
                        jEntity.Add("dc_afternoonsnack", u.AfternoonSnack);
                    }
                    if (u.EveningSnack != null)
                    {
                        jEntity.Add("dc_eveningsnack", u.EveningSnack);
                    }
                    if (u.UserSpecifiedKcalTarget != null)
                    {
                        jEntity.Add("dc_userspecifiedkcaltarget", u.UserSpecifiedKcalTarget);
                    }
                    //Guids
                    if (u.MenuPresetId != Guid.Empty)
                    {
                        //jEntity.Add("dc_menupresetid", u.MenuPresetId);
                        jEntity.Add("dc_menupresetid@odata.bind", "/dc_presetses(" + u.MenuPresetId + ")");
                    }
                    if (u.MenuId != Guid.Empty)
                    {
                        jEntity.Add("dc_grocerprimaryid@odata.bind", "/dc_menus(" + u.MenuId + ")");
                    }
                    if (u.GrocerPrimaryId != Guid.Empty)
                    {
                        jEntity.Add("dc_grocerprimaryid@odata.bind", "/dc_grocers(" + u.GrocerPrimaryId + ")");
                    }

                    if (u.GrocerSecondaryId != Guid.Empty)
                    {
                        jEntity.Add("dc_grocersecondaryid@odata.bind", "/dc_grocers(" + u.GrocerSecondaryId + ")");
                    }

                    if (u.GrocerTertiaryId != Guid.Empty)
                    {
                        jEntity.Add("dc_grocertertiaryid@odata.bind", "/dc_grocers(" + u.GrocerTertiaryId + ")");
                    }
                    if (u.CountryId != Guid.Empty)
                    {
                        jEntity.Add("dc_countryid@odata.bind", "/dc_countries(" + u.CountryId + ")");
                    }

                    if (u.VerificationCodeId != Guid.Empty)
                    {
                        jEntity.Add("dc_verifycustomerid@odata.bind", "/dc_verifycustomers(" + u.VerificationCodeId + ")");
                    }


                }
                return (jEntity);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: Convert<T>(RegardingTypeCode, T)", err);
            }
            return (null);
        }

        /// <summary>
        /// Can return a single object or a List<> of objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="regardingObjectType"></param>
        /// <param name="jObject"></param>
        /// <returns></returns>
        public dynamic Execute<T>(DynamicsConnection.RegardingTypeCode regardingObjectType, JObject jObject)
        {
            switch(regardingObjectType)
            {
                case DynamicsConnection.RegardingTypeCode.Account:
                    {

                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.Contact:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<User>();
                            foreach(var val in valArray)
                            {
                                var user = BuildUser((JObject)val);
                                list.Add(user);
                            }
                            if(typeof(T) == typeof(User))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var user = BuildUser(jObject);
                            return (user);
                        }

                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.Preset:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<Preset>();
                            foreach (var val in valArray)
                            {
                                var preset = BuildPreset((JObject)val);
                                list.Add(preset);
                            }
                            if (typeof(T) == typeof(Preset))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildPreset(jObject);
                            return (preset);
                        }
                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.Country:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<Country>();
                            foreach (var val in valArray)
                            {
                                var preset = BuildCountry((JObject)val);
                                list.Add(preset);
                            }
                            if (typeof(T) == typeof(Country))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildCountry(jObject);
                            return (preset);
                        }
                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.Grocer:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<Grocer>();
                            foreach (var val in valArray)
                            {
                                var grocer = BuildGrocer((JObject)val);
                                list.Add(grocer);
                            }
                            if (typeof(T) == typeof(Grocer))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildCountry(jObject);
                            return (preset);
                        }
                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.VerifyCustomer:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<VerifyCustomer>();
                            foreach (var val in valArray)
                            {
                                var vc = BuildVerifyCustomer((JObject)val);
                                list.Add(vc);
                            }
                            if (typeof(T) == typeof(VerifyCustomer))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildVerifyCustomer(jObject);
                            return (preset);
                        }
                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.FoodLike:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<FoodLike>();
                            foreach (var val in valArray)
                            {
                                var vc = BuildFoodLike((JObject)val);
                                list.Add(vc);
                            }
                            if (typeof(T) == typeof(FoodLike))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildFoodLike(jObject);
                            return (preset);
                        }
                    }
                    break;
                case DynamicsConnection.RegardingTypeCode.FoodDislike:
                    {
                        JToken valArray;
                        if (jObject.TryGetValue("value", out valArray))
                        {
                            var list = new List<FoodDislike>();
                            foreach (var val in valArray)
                            {
                                var vc = BuildFoodDislike((JObject)val);
                                list.Add(vc);
                            }
                            if (typeof(T) == typeof(FoodDislike))
                            {
                                return (list[0]);
                            }
                            return (list);
                        }
                        else
                        {
                            var preset = BuildFoodLike(jObject);
                            return (preset);
                        }
                    }
                    break;

            }

            return (null);
        }

        private FoodDislike BuildFoodDislike(JObject tolken)
        {
            try
            {
                var fd= new FoodDislike()
                {
                    FoodDislikeId = RetrieveValue<Guid>(tolken, "dc_fooddislikeid"),
                    Name = RetrieveValue<String>(tolken, "dc_foods_x002e_dc_name"),
                    FoodId = RetrieveValue<Guid>(tolken, "_dc_foodid_value"),
                    MealComponentId = RetrieveValue<Guid>(tolken, "_dc_mealcompentid_value"),
                };
                var componentName = RetrieveValue<String>(tolken, "dc_mealcomponent_x002e_dc_name");
                if(!String.IsNullOrEmpty(componentName))
                {
                    fd.Name = componentName;
                }
                return (fd);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildFoodDislike(JObject)", err);
            }
            return (null);
        }

        private FoodLike BuildFoodLike(JObject tolken)
        {
            try
            {
                var fl = new FoodLike()
                {
                    FoodLikeId = RetrieveValue<Guid>(tolken, "dc_foodlikeid"),
                    Name = RetrieveValue<String>(tolken, "dc_foods_x002e_dc_name"),
                    Day = RetrieveValue<int>(tolken, "dc_day"),
                    Meal = RetrieveValue<int>(tolken, "dc_meal"),
                    FoodId = RetrieveValue<Guid>(tolken, "_dc_foodid_value"),
                };
                return (fl);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildFoodLike(JObject)", err);
            }
            return (null);
        }

        private VerifyCustomer BuildVerifyCustomer(JObject tolken)
        {
            try
            {
                var vc = new VerifyCustomer()
                {
                    VerifyCustomerId = RetrieveValue<Guid>(tolken, "dc_verifycustomerid"),
                    Name = RetrieveValue<String>(tolken, "dc_name"),

                };
                return (vc);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildVerifyCustomer(JObject)", err);
            }
            return (null);
        }

        private Grocer BuildGrocer(JObject tolken)
        {
            try
            {
                var g = new Grocer()
                {
                    GrocerId = RetrieveValue<Guid>(tolken, "dc_grocerid"),
                    Name = RetrieveValue<String>(tolken, "dc_name"),

                };
                return (g);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildGrocer(JObject)", err);
            }
            return (null);
        }

        private Country BuildCountry(JObject tolken)
        {
            try
            {
                var c = new Country()
                {
                    CountryId = RetrieveValue<Guid>(tolken, "dc_countryid"),
                    Name = RetrieveValue<String>(tolken, "dc_name"),

                };
                return (c);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildCountry(JObject)", err);
            }
            return (null);
        }

        private Preset BuildPreset(JObject tolken)
        {
            try
            {
                var p = new Preset()
                {
                    PresetId        = RetrieveValue<Guid>(tolken, "dc_presetsid"),
                    Name            = RetrieveValue<String>(tolken, "dc_name"),
                    Description     = RetrieveValue<String>(tolken, "dc_description"),
                    ImageURL        = RetrieveValue<String>(tolken, "dc_externalimageurl"),
                };
                return (p);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildPreset(JObject)", err);
            }
            return (null);
        }


        private User BuildUser(JObject tolken)
        {
            try
            {
                var u = new User()
                {
                    MenuId = RetrieveValue<Guid>(tolken, "dc_menu_x002e_dc_menuid"),
                    UserId = RetrieveValue<Guid>(tolken, "contactid"),
                    FirstName = RetrieveValue<String>(tolken, "firstname"),
                    EmailAddress1 = RetrieveValue<String>(tolken, "emailaddress1"),
                    LastName = RetrieveValue<String>(tolken, "lastname"),
                    GenderCode = RetrieveValue<int>(tolken, "gendercode"),
                    TargetWeight = RetrieveValue<int>(tolken, "dc_targetweight"),
                    CurrentWeight = RetrieveValue<int>(tolken, "dc_currentweight"),
                    HeightFeet = RetrieveValue<int>(tolken, "dc_heightfeet"),
                    HeightInches = RetrieveValue<int>(tolken, "dc_heightinches"),
                    Birthdate = RetrieveValue<DateTime>(tolken, "birthdate"),
                    ActivityLevel = RetrieveValue<int>(tolken, "dc_activitylevel"),
                    PoundsPerWeek = RetrieveValue<int>(tolken, "dc_poundsperweek"),
                    MaintainTargetWeight = RetrieveValue<bool>(tolken, "dc_maintaintargetweight"),
                    KCalCalculatedtarget = RetrieveValue<int>(tolken, "dc_kcalcalculatedtarget"),
                    WeightKG = RetrieveValue<int>(tolken, "dc_weightkg"),
                    HeightCM = RetrieveValue<int>(tolken, "dc_heightcm"),
                    Age = RetrieveValue<int>(tolken, "dc_age"),
                    BMI = RetrieveValue<int>(tolken, "dc_bmi"),

                    MorningSnack = RetrieveValue<bool>(tolken, "dc_morningsnack"),
                    AfternoonSnack = RetrieveValue<bool>(tolken, "dc_afternoonsnack"),
                    EveningSnack = RetrieveValue<bool>(tolken, "dc_eveningsnack"),
                    MenuPresetId = RetrieveValue<Guid>(tolken, "_dc_menupresetid_value"),
                    DEE = RetrieveValue<int>(tolken, "dc_dee"),
                    UserSpecifiedKcalTarget = RetrieveValue<bool>(tolken, "dc_userspecifiedkcaltarget"),
                    KcalTarget  = RetrieveValue<int>(tolken, "dc_kcaltarget"),
                };
                return (u);
            }catch(Exception err)
            {
                Globals.LogHelper.CreateLog("ConvertFrom.cs: BuildUser(JObject)", err);
            }
            return (null);
        }

        private T RetrieveValue<T>(JObject entity, String prop)
        {
            
            if (entity[prop] != null)
            {
                var propValue = entity[prop].ToString();
                if (!propValue.Equals(String.Empty))
                {
                    if (typeof(T) == typeof(Guid))
                    {
                        var g = new Guid();
                        if (Guid.TryParse(propValue, out g))
                        {
                            var rt = (T)Convert.ChangeType(g, typeof(T), new CultureInfo("en-US"));
                            return (rt);
                        }
                    }
                    else if (typeof(T) == typeof(int))
                    {
                        var g = new int();
                        if (int.TryParse(propValue, out g))
                        {
                            var rt = (T)Convert.ChangeType(g, typeof(T), new CultureInfo("en-US"));
                            return (rt);
                        }
                    }
                    else if (typeof(T) == typeof(String))
                    {
                        var rt = (T)Convert.ChangeType(propValue, typeof(T), new CultureInfo("en-US"));
                        return (rt);
                    }
                    else if (typeof(T) == typeof(double))
                    {
                        var g = new int();
                        if (int.TryParse(propValue, out g))
                        {
                            var rt = (T)Convert.ChangeType(g, typeof(T), new CultureInfo("en-US"));
                            return (rt);
                        }
                    }
                    else if (typeof(T) == typeof(DateTime))
                    {
                        var g = new DateTime();
                        if (DateTime.TryParse(propValue, out g))
                        {
                            var rt = (T)Convert.ChangeType(g, typeof(T), new CultureInfo("en-US"));
                            return (rt);
                        }
                    }
                    else if (typeof(T) == typeof(bool))
                    {
                        var g = new bool();
                        if (bool.TryParse(propValue, out g))
                        {
                            var rt = (T)Convert.ChangeType(g, typeof(T), new CultureInfo("en-US"));
                            return (rt);
                        }
                    }
                }
            }
            return (default(T));
        }
        /*
        public dynamic Execute<T>(T entity)
        {
            Entity crmEntity = null;

            if (entity is Entity)
            {
                crmEntity = (Entity)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
            }

            if (entity is User)
            {
                var obj = (User)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
                var dr = new Entity("contact")
                {
                    Id = obj.UserId,
                    ["firstname"] = obj.FirstName,
                    ["lastname"] = obj.LastName,
                    ["emailaddress1"] = obj.EmailAddress1,
                };
                return (dr);
            }

            else if (entity is Entity && crmEntity.LogicalName.Equals("contact"))
            {
                //var obj = (sm_DiscoveryRequest)Convert.ChangeType(entity, entity.GetType(), new CultureInfo("en-US"));
                var dr = new User()
                {
                    UserId = crmEntity.Id,
                    FirstName = (String)crmEntity["firstname"],
                    LastName = (String)crmEntity["lastname"],
                    EmailAddress1 = (String)crmEntity["emailaddress1"]
                };
                return (dr);
            }

            return (default(T));
        }
        */
    }
}
