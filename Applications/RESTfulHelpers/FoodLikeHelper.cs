﻿using DynamicConnections.Nutristyle.Dynamics8Helpers;
using Newtonsoft.Json.Linq;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public class FoodLikeHelper
    {
        /// <summary>
        /// Retrieve the foodlikes for the user in question
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<FoodLike> RetrieveFoodLikes(Guid userId)
        {
            try
            {

                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                  <entity name='dc_foodlike'>
                    <attribute name='dc_day' />
                    <attribute name='dc_meal' />
                    <attribute name='dc_foodid' />
                    <attribute name='dc_foodlikeid' />
                    <order attribute='dc_foodid' descending='false' />
                    <filter type='and'>
                        <condition attribute='dc_contactid' value='@CONTACTID' operator='eq'/>
                    </filter>
                    <link-entity name='dc_foods' from='dc_foodsid' to='dc_foodid' alias='dc_foods'>
                        <attribute name='dc_name' />
                    </link-entity>
                  </entity>
                </fetch>";

                fetchXml = fetchXml.Replace("@CONTACTID", userId.ToString());

                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.FoodLike, fetchXml).Result;
                var cf = new ConvertFrom();
                var foodLikes = (List<FoodLike>)cf.Execute<List<FoodLike>>(DynamicsConnection.RegardingTypeCode.FoodLike, results);
                
                return (foodLikes);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodlikeHelper.cs: RetrieveFoodLikes()", err);
            }

            return (null);
        }
        
        /// <summary>
        /// Creates a foodlike.
        /// </summary>
        /// <param name="fl"></param>
        /// <returns></returns>
        public Guid CreateFoodLike(FoodLike fl)
        {
            try
            {
                var entity = BuildJObject(fl);

                var dc = new DynamicsConnection();
                var Id = dc.Create(DynamicsConnection.RegardingTypeCode.FoodLike, entity).Result;
                return (Id);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: CreateFoodLike(FoodLike)", err);
            }
            return (Guid.Empty);
        }
        
        public bool UpdateFoodLike(FoodLike fl)
        {
            try
            {
                var entity = BuildJObject(fl);
                var dc = new DynamicsConnection();
                var Id = dc.Update(DynamicsConnection.RegardingTypeCode.FoodLike, fl.FoodLikeId, entity).Result;
                return (true);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: UpdateFoodLike(FoodLike)", err);
            }

            return (false);
        }

        public bool DeleteFoodLike(Guid foodLikeId)
        {
            try
            {
                var dc = new DynamicsConnection();
                var result = dc.Delete(DynamicsConnection.RegardingTypeCode.FoodLike, foodLikeId).Result;
                return (result);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodLikeHelper.cs: DeleteFoodLike(Guid)", err);
            }

            return (false);
        }

        private JObject BuildJObject(FoodLike fl)
        {
            var entity = new JObject
                {
                    { "dc_day", fl.Day },
                    { "dc_meal", fl.Meal },
                };
            if (fl.UserId != Guid.Empty)
            {
                entity.Add("dc_contactid@odata.bind", "/contacts(" + fl.UserId + ")");
            }
            if (fl.FoodId != Guid.Empty)
            {
                entity.Add("dc_foodid@odata.bind", "/dc_foodses(" + fl.FoodId + ")");
            }
            /*
            if(fl.FoodLikeId != Guid.Empty)
            {
                entity.Add("dc_foodlikeid", fl.FoodLikeId);
            }*/
            return (entity);
        }

    }
}
