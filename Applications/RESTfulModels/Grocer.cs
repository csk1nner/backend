﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels
{
    public class Grocer
    {
        public Guid GrocerId { get;set;}
        public String Name { get; set; }
    }
}
