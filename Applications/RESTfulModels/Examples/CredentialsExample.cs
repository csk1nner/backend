﻿using Swashbuckle.AspNetCore.Examples;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels.Examples
{
    public class CredentialsExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new Credentials
            {
                Email = "email@test.com",
                Password = "Password",
                
            };
        }
    }
}
