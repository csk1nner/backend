﻿using Swashbuckle.AspNetCore.Examples;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels.Examples
{
    public class AccountExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new Account
            {
                Email = "email@test.com",
                Password = "Password",
                UserType = 1,
            };
        }
    }
}
