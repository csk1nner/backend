﻿using Swashbuckle.AspNetCore.Examples;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels.Examples
{
    public class UserExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new User
            {
                FirstName = "FirstName",
                LastName = "LastName",
                EmailAddress1 = "test@test.com",
                UserSpecifiedKcalTarget = true,
                MorningSnack = false,
                AfternoonSnack = true,
                EveningSnack = false,
                MaintainTargetWeight = false,
            };
        }
    }
}
