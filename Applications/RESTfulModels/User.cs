﻿using Swashbuckle.AspNetCore.Examples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NutriStyle.WebAPI.RESTfulModels
{
    /// <summary>
    /// A User is a contact in CRM that allows for the generation of menus, etc
    /// </summary>
    public class User 
    {
        public String FirstName { get; set; }
        public String EmailAddress1 { get; set; }
        public String LastName { get; set; }
        public int GenderCode { get; set; }
        public int CurrentWeight { get; set; }
        public int HeightFeet { get; set; }
        public int HeightInches { get; set; }
        public DateTime Birthdate { get; set; }
        public int ActivityLevel { get; set; }
        public int PoundsPerWeek { get; set; }
        public bool? MaintainTargetWeight { get; set; }
        public int KCalCalculatedtarget { get; set; }
        public int WeightKG { get; set; }
        public int HeightCM { get; set; }
        public int Age { get; set; }
        public int BMI { get; set; }
        public Guid UserId { get; set; }
        public bool? MorningSnack { get; set; }
        public bool? AfternoonSnack { get; set; }
        public bool? EveningSnack { get; set; }
        public Guid MenuPresetId { get; set; }
        public int DEE { get; set; }
        public bool? UserSpecifiedKcalTarget { get; set; }
        public int KcalTarget { get; set; }
        public Guid RollShoppingListToParent { get; set; }

        public int TargetWeight { get; set; }

        public Guid MenuId { get; set; }
        //public Guid MenuPresetId { get; set; }

        public Guid GrocerPrimaryId { get; set; }
        public Guid GrocerSecondaryId { get; set; }
        public Guid GrocerTertiaryId { get; set; }
        public Guid CountryId { get; set; }
        public Guid VerificationCodeId { get; set; }





    }
}
