﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels
{
    public class FoodLike
    {
        public Guid FoodLikeId { get;set;}
        public String Name { get; set; }
        public int Day { get; set; }
        public int Meal { get; set; }
        /// <summary>
        /// dc_foodsId
        /// </summary>
        public Guid FoodId { get; set; }
        public Guid UserId { get; set; }
    }
}
