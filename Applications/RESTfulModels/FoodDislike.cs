﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels
{
    public class FoodDislike
    {
        public Guid FoodDislikeId { get;set;}
        public String Name { get; set; }
        public Guid FoodId { get; set; }
        public Guid UserId { get; set; }
        public Guid MealComponentId { get; set; }
    }
}
