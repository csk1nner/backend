﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulModels
{
    public class Preset
    {
        public Guid PresetId {get;set;}
        public String Name { get; set; }
        public String Description { get; set; }
        public String ImageURL { get; set; }
    }
}
