﻿using NutriStyle.WebAPI.RESTfulHelpers;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulAbstractClasses
{
    public class LogHelper : NutriStyle.WebAPI.RESTfulHelpers.LogHelperBase
    { 
        private readonly static object WriteLock = new object();
        private String logName;

        //OrganizationServiceProxy serviceProxy;
        //Datastore ds = null;
        public LogHelper()
        {
            logName = "NutriStyle.txt";
        }
        /*
        public LogHelper(Datastore ds) : this()
        {

            //serviceProxy.EnableProxyTypes();
            //this.serviceProxy = serviceProxy;
            this.ds = ds;
        }
        */
        
        public override void Create(Log l)
        {
            if (true)//(Globals.IsLogEnabled)
            {
                /*
                var path = BitAssassins.ProjectXIII.Helpers.Globals.Path + BitAssassins.ProjectXIII.Helpers.Globals.Separator + "Project XIII" +
                           BitAssassins.ProjectXIII.Helpers.Globals.Separator + "Logs";
                */

                
                var path = @"c:\windows\temp\";//Globals.LogPath;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path += Globals.Separator + logName;//"PXIIILog.txt";
                    lock (WriteLock)
                    {
                        using (var stream = new StreamWriter(path, true))
                        {
                            stream.WriteLine(l.CreatedOn + "|" + l.Source + "|" + l.Message + "|" + l.StackTrace);
                        }
                    }
                //Check the file size

                if (File.Exists(path))
                {
                    long length = new FileInfo(path).Length;
                    if (length > 1024 * 1000) //one k
                    {
                        var newPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Globals.Separator +
                                      "Project XIII" + Globals.Separator + "Logs" + Globals.Separator + logName + "-" +
                                      DateTime.UtcNow.Ticks + ".txt";
                        File.Move(path, newPath);
                    }
                }
                
                //ds.Insert(l);
            }
        }

        public override void Cleanup()
        {
            //Remove all the log- files
            try
            {
                /*
                var path = Globals.LogPath;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path += Globals.Separator;
                var files = Directory.EnumerateFiles(path, logName+"-*.txt");
                foreach (var f in files)
                {
                    File.Delete(f);
                }
                */
            }
            catch (Exception err)
            {
                CreateLog("LogHelper.cs: Cleanup()", err);
            }
        }
    }
}
