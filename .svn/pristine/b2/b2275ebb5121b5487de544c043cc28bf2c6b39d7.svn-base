﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NutriStyle.WebAPI.RESTfulAbstractClasses;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.HttpOverrides;
using Swashbuckle.AspNetCore.Examples;
using System;

namespace utriStyle.WebAPI.RESTful
{
    //Enable CORS
    //https://stackoverflow.com/questions/31942037/how-to-enable-cors-in-asp-net-core#31942128
    //Temp data provider & Session State
    //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/app-state?tabs=aspnetcore2x

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public IConfiguration Configuration { get; }

        protected static string GetXmlCommentsPath()
        {
            return String.Format(@"{0}NutriStyle.WebAPI.RESTful.xml", System.AppDomain.CurrentDomain.BaseDirectory);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            

            services.AddSwaggerGen(options =>
            {
                options.IncludeXmlComments(GetXmlCommentsPath());
                options.OperationFilter<ExamplesOperationFilter>();
                options.SwaggerDoc("v1", new Info
                {
                    Title = "NutriStyle.WebAPI.RESTful Help",
                    Version = "v1"
                });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            
            Globals.Separator = Path.DirectorySeparatorChar;
          
            Globals.LogHelper = new LogHelper();

            services.AddMvc();

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(60*30); //30 minutes.  //TODO: Add static readonly to match this  to the JWT
                options.Cookie.HttpOnly = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseCors("AllowAll");
            app.UseSwagger();
            
           
            app.UseSwaggerUI(options =>
            {
                
                options.SwaggerEndpoint(
                  "/swagger/v1/swagger.json", "NutriStyle.WebAPI.RESTful v1 Endpoint");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();

            app.UseSession();
            app.UseMvcWithDefaultRoute();

            app.UseMvc();
        }
    }
}
