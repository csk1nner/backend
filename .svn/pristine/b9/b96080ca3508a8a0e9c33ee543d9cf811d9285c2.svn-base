﻿using DynamicConnections.Nutristyle.Dynamics8Helpers;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    public class DropdownHelper
    {
        /// <summary>
        /// Retrieve the meal types.  Global picklist
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveMealTypes()
        {
            try
            {
                //http://crmdev.dynamiconnections.com:5555/NS/api/data/v8.2/GlobalOptionSetDefinitions?$select=Name
                var dc = new DynamicsConnection();
                var list = dc.QueryPicklist(new Guid("bad9dcaf-f3ff-e011-ba65-00155d0a0205")).Result;
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveMealTypes()", err);
            }
            return (null);
        }
        // <summary>
        /// Retrieve the day types from the dc_dayoptionset global picklist
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveDayTypes()
        {
            try
            {
                var dc = new DynamicsConnection();
                var list = dc.QueryPicklist(new Guid("f8089d72-f3ff-e011-ba65-00155d0a0205")).Result;
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveDayTypes()", err);
            }

            return (null);
        }

        /// <summary>
        /// Retrieve the gender options from the contact entity
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveGender()
        {
            try
            {
                var dc = new DynamicsConnection();
                var list = dc.QueryPicklist(new Guid("608861BC-50A4-4C5F-A02C-21FE1943E2CF"), new Guid("A81E86C9-E2E1-4C1E-81AB-8913EEDAAB47")).Result;
                return (list);
               
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveGender()", err);
            }

            return (null);
        }

        /// <summary>
        /// Retrieve the dc_heightfeet options from the contact entity
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveHeightFeet()
        {
            try
            {
                var dc = new DynamicsConnection();
                var list = dc.QueryPicklist(new Guid("608861BC-50A4-4C5F-A02C-21FE1943E2CF"), new Guid("CF8A2837-63F9-E011-8A4C-00155D0A0205")).Result;
                return (list);
                
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveHeightFeet()", err);
            }

            return (null);
        }

        /// <summary>
        /// Retrieve the dc_heightinches  from the contact entity
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveHeightInches()
        {
            try
            {
                var dc = new DynamicsConnection();
                var list = dc.QueryPicklist(new Guid("608861BC-50A4-4C5F-A02C-21FE1943E2CF"), new Guid("DC093B71-63F9-E011-8A4C-00155D0A0205")).Result;
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveHeightInches()", err);
            }

            return (null);
        }

        /// <summary>
        /// Retrieve the dc_countryid and the dc_name values of all the dc_country records
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveCountries()
        {
            try
            {
              
                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_country'>
                        <attribute name='dc_countryid' />
                        <attribute name='dc_name' />
                        <filter type='and'>
                            <condition attribute='statecode' operator='eq' value='0' />
                        </filter>
                        <order descending ='false' attribute='dc_name' />
                      </entity>
                    </fetch>";


                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.Country, fetchXml).Result;
                var cf = new ConvertFrom();
                var countries = (List<Country>)cf.Execute<List<Country>>(DynamicsConnection.RegardingTypeCode.Country, results);
                var list = new List<KeyValuePair<String, String>>();
                foreach(var c in countries)
                {
                    list.Add(new KeyValuePair<string, string>(c.CountryId.ToString(), c.Name));
                }
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveCounties()", err);
            }

            return (null);
        }
        public List<KeyValuePair<String, String>> RetrieveVerifyCustomers()
        {
            try
            {
                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_verifycustomer'>
                        <attribute name='dc_verifycustomerid' />
                        <attribute name='dc_name' />
                        <filter type='and'>
                            <condition attribute='statecode' operator='eq' value='0' />
                        </filter>
                        <order descending ='false' attribute='dc_name' />
                      </entity>
                    </fetch>";
                
                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.VerifyCustomer, fetchXml).Result;
                var cf = new ConvertFrom();
                var verifyCustomers = (List<VerifyCustomer>)cf.Execute<List<Country>>(DynamicsConnection.RegardingTypeCode.VerifyCustomer, results);
                var list = new List<KeyValuePair<String, String>>();
                foreach (var c in verifyCustomers)
                {
                    list.Add(new KeyValuePair<string, string>(c.VerifyCustomerId.ToString(), c.Name));
                }
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveVerifyCustomers()", err);
            }

            return (null);
        }
        /// <summary>
        /// Retrieve the dc_grocerid and the dc_name values of all the dc_grocer records
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<String, String>> RetrieveGrocers()
        {
            try
            {

                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_grocer'>
                        <attribute name='dc_grocerid' />
                        <attribute name='dc_name' />
                        <filter type='and'>
                            <condition attribute='statecode' operator='eq' value='0' />
                        </filter>
                        <order descending ='false' attribute='dc_name' />
                      </entity>
                    </fetch>";


                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.Grocer, fetchXml).Result;
                var cf = new ConvertFrom();
                var groceries = (List<Grocer>)cf.Execute<List<Country>>(DynamicsConnection.RegardingTypeCode.Grocer, results);
                var list = new List<KeyValuePair<String, String>>();
                foreach (var c in groceries)
                {
                    list.Add(new KeyValuePair<string, string>(c.GrocerId.ToString(), c.Name));
                }
                return (list);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveGrocers()", err);
            }

            return (null);
        }

        /// <summary>
        /// These types are not live.  Account is the most basic and highest type of user.  The idea is that someone
        /// can be a consumer of NutriStyle and a healthcare professional
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<int, String>> RetrieveAccountTypes()
        {
            try
            {
                var list = new List<KeyValuePair<int, String>>();
                list.Add(new KeyValuePair<int, string>(1, "Consumer"));
                list.Add(new KeyValuePair<int, string>(2, "Healthcare professional"));
                list.Add(new KeyValuePair<int, string>(3, "Grocer"));
                return (list);

            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveAccountType()", err);
            }
            return (null);
        }


        /// <summary>
        /// Retrieve the active preset records.  The filter is an int and fuctions as follows:  There are  five
        /// filter types: None, Vegan, Vegeterian,  Gluten Free & Drairy Free.  The on/off are represented by a 1 or zero
        /// so 00000 is all off.  10000 is None, 01000 is Vegan,  etc
        /// </summary>
        /// <returns></returns>
        public List<Preset> RetrievePresets(String filter)
        {
            try
            {
                var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_presets'>
                        <attribute name='dc_presetsid' />
                        <attribute name='dc_name' />
                        <attribute name='dc_description' />
                        <attribute name='dc_externalimageurl'/>
                        <filter type='and'>
                            <condition attribute='statecode' operator='eq' value='0' />

                            @PRESETFILTER

                            

                        </filter>
                        <order descending ='false' attribute='dc_name' />
                      </entity>
                    </fetch>";


                /*
                 <condition attribute='dc_none' operator='@NONE' value='1' />
                            <condition attribute='dc_vegan' operator='@VEGAN' value='1' />
                            <condition attribute='dc_vegetarian' operator='@VEGETARIAN' value='1' />
                            <condition attribute='dc_glutenfree' operator='@GLUTENFREE' value='1' />
                            <condition attribute='dc_dairyfree' operator='@DAIRYFREE' value='1' />
                 */
                var filterString = new StringBuilder();
                if (String.IsNullOrEmpty (filter))
                {
                    fetchXml = fetchXml.Replace("@PRESETFILTER", String.Empty);
                    /*
                    fetchXml.Replace("@NONE", "ne");
                    fetchXml.Replace("@VEGAN", "ne");
                    fetchXml.Replace("@VEGETARIAN", "ne");
                    fetchXml.Replace("@GLUTENFREE", "ne");
                    fetchXml.Replace("@DAIRYFREE", "ne");*/
                }
                

                else if(filter.ToString().Length == 5)
                {
                    var none        = Convert.ToInt32(filter.ToString().Substring(4, 1));
                    var vegan       = Convert.ToInt32(filter.ToString().Substring(3, 1));
                    var vegetarian  = Convert.ToInt32(filter.ToString().Substring(2, 1));
                    var glutenFree  = Convert.ToInt32(filter.ToString().Substring(1, 1));
                    var dairyFree   = Convert.ToInt32(filter.ToString().Substring(0, 1));

                    var noneFilter = String.Empty;
                    var veganFilter = String.Empty;
                    var vegetarianFilter = String.Empty;
                    var glutenFreeFilter = String.Empty;
                    var dairyFreeFilter = String.Empty;

                    if (none == 1)
                    {
                        noneFilter = "<condition attribute='dc_none' operator='eq' value='1' />";
                    }
                    else
                    {
                        //noneFilter = "<condition attribute='dc_none' operator='ne' value='1' />";
                    }

                    if (vegan == 1)
                    {
                        veganFilter = "<condition attribute='dc_vegan' operator='eq' value='1' />";
                    }
                    else
                    {
                        veganFilter = "<condition attribute='dc_vegan' operator='ne' value='1' />";
                    }

                    if (vegetarian == 1)
                    {
                        vegetarianFilter = "<condition attribute='dc_vegetarian' operator='eq' value='1' />";
                    }
                    else
                    {
                        vegetarianFilter = "<condition attribute='dc_vegetarian' operator='ne' value='1' />";
                    }

                    if (glutenFree == 1)
                    {
                        glutenFreeFilter = "<condition attribute='dc_glutenfree' operator='eq' value='1' />";
                    }
                    else
                    {
                        glutenFreeFilter = "<condition attribute='dc_glutenfree' operator='ne' value='1' />";
                    }

                    if (dairyFree == 1)
                    {
                        dairyFreeFilter = "<condition attribute='dc_dairyfree' operator='eq' value='1' />";
                    }
                    else
                    {
                        //dairyFreeFilter = "<condition attribute='dc_dairyfree' operator='ne' value='1' />";
                    }

                    filterString.AppendLine(noneFilter);
                    filterString.AppendLine(veganFilter);
                    filterString.AppendLine(vegetarianFilter);
                    filterString.AppendLine(glutenFreeFilter);
                    filterString.AppendLine(dairyFreeFilter);
                    fetchXml = fetchXml.Replace("@PRESETFILTER", filterString.ToString());
                }
                      
                


                var dc = new DynamicsConnection();
                var results = dc.FetchXML(DynamicsConnection.RegardingTypeCode.Preset, fetchXml).Result;
                var cf = new ConvertFrom();
                var presets = (List<Preset>)cf.Execute<List<Preset>>(DynamicsConnection.RegardingTypeCode.Preset, results);
               
                return (presets);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("DropdownHelper.cs: RetrieveGrocers()", err);
            }

            return (null);
        }


    }
}
