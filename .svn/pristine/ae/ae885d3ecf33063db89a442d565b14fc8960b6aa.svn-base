﻿using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Identity;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using NutriStyle.WebAPI.RESTfulModels;
using Swashbuckle.AspNetCore.Examples;
using NutriStyle.WebAPI.RESTfulModels.Examples;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using NutriStyle.WebAPI.RESTful.Extensions;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace NutriStyle.WebAPI.RESTful.Controllers
{

    [Route("api/[controller]")]
    public class FoodDislikeController : BaseController
    {
        private IConfiguration config;
               
        public FoodDislikeController(IConfiguration config)
        {
            this.config = config;
        }

        /// <summary>
        /// Retrieve the food dislikes for the user.  UserId is provided by Session management
        /// </summary>
        /// <returns></returns>
        /// /// <response code="200">Returns list of food dislikes</response>
        /// <response code="400">If get fails</response>    
        [HttpGet("Get"), Authorize]
        public IActionResult Get()
        {
            try
            {
                var currentUser = HttpContext.User;

                if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                {
                    var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                    var flh = new FoodDislikeHelper();
                    var list = flh.RetrieveFoodDislikes(userId);
                    var response = Ok(list);
                    return response;
                }
                return Error("FoodDislikeController.cs: Retrieve(): UserId value is Guid.Empty");
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodDislikeController.cs: Retrieve()", err);
            }

            return Error("FoodDislikeController.cs: Retrieve()");
        }

        /// <summary>
        /// Returns Guid of the created food dislike.  Writes over the UserId attribute with the value from the  claim
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        /// <response code="200">Returns Guid</response>
        /// <response code="400">If create fails</response>    
        [HttpPost("Create"), Authorize]
        [SwaggerRequestExample(typeof(FoodDislike), typeof(FoodDislikeExample))]
        public IActionResult Create([FromBody] FoodDislike fd)
        {
            var flh = new FoodDislikeHelper();
            try
            {
                if (fd != null)
                {
                    var currentUser = HttpContext.User;

                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                        fd.UserId = userId;
                        var Id = flh.CreateFoodDislike(fd);
                        var response = Ok(new { Id });
                        return response;
                    }
                    return Error("FoodDislikeController.cs: Create(FoodDislike): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodDislikeController.cs: Create(FoodDislike)", err);
                return Error("FoodDislikeController.cs: Create(FoodDislike): " + err.Message);
            }
            return Error("FoodDislikeController.cs: Create(FoodDislike): Unknown error");
        }
        /// <summary>
        /// Update the food dislike
        /// </summary>
        /// <param name="fooddislike"></param>
        /// <returns></returns>
        /// <response code="200">Returns bool based on success or failure</response>
        /// <response code="400">If update fails</response>    
        [HttpPatch("Update"), Authorize]
        [SwaggerRequestExample(typeof(FoodDislike), typeof(FoodDislikeExample))]
        public IActionResult Update([FromBody] FoodDislike fd)
        {
            var fdh = new FoodDislikeHelper();
            try
            {
                if (fdh != null)
                {
                    var currentUser = HttpContext.User;

                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                        fd.UserId = userId;
                        var result = fdh.UpdateFoodDislike(fd);
                        var response = Ok(new { result });
                        return response;
                    }
                    return Error("FoodDislikeController.cs: Update(FoodDislike): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodDislikeController.cs: Update(FoodDislike)", err);
                return Error("FoodDislikeController.cs: Update(FoodDislike): " + err.Message);
            }
            return Error("FoodDislikeController.cs: Update(FoodDislike): Unknown error");
        }

        /// <summary>
        /// Delete the food dislike the passed in Guid represents
        /// </summary>
        /// <param name="foodDislikeId"></param>
        /// <returns></returns>
        /// <response code="200">Returns bool based on success or failure</response>
        /// <response code="400">If delete fails</response>    
        [HttpDelete("Delete/{fooddislikeid}"), Authorize]
        public IActionResult Delete(Guid foodDislikeId)
        {
            var flh = new FoodLikeHelper();
            try
            {
                if (foodDislikeId != Guid.Empty)
                {
                    var currentUser = HttpContext.User;

                    //Todo: Validate that the user owns the foodlike before deleting
                    if (currentUser.HasClaim(c => c.Type == ClaimTypes.PrimarySid))
                    {
                        var userId = new Guid(currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value);
                       
                        var result = flh.DeleteFoodLike(foodDislikeId);
                        var response = Ok(new { result });
                        return response;
                    }
                    return Error("FoodDislikeController.cs: Delete(Guid): UserId value is Guid.Empty");
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("FoodDislikeController.cs: Delete(Guid)", err);
                return Error("FoodDislikeController.cs: Delete(Guid): " + err.Message);
            }
            return Error("FoodDislikeController.cs: Delete(Guid): Unknown error");
        }

    }
}
