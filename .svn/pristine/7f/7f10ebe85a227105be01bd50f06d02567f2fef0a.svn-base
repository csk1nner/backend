﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using JWT;
using JWT.Serializers;
using JWT.Algorithms;
using NutriStyle.WebAPI.RESTfulHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using NutriStyle.WebAPI.RESTfulModels;
using Swashbuckle.AspNetCore.Examples;
using NutriStyle.WebAPI.RESTfulModels.Examples;

namespace NutriStyle.WebAPI.RESTful.Controllers
{
    //https://auth0.com/blog/asp-dot-net-core-authentication-tutorial/
    //https://auth0.com/blog/securing-asp-dot-net-core-2-applications-with-jwts/
    //https://stormpath.com/blog/tutorial-policy-based-authorization-asp-net-core
    //http://blog.getpostman.com/2014/01/27/extracting-data-from-responses-and-chaining-requests/

    [Route("api/[controller]")]
    public class UserController : BaseController
    {

        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly JWTSettings options;
        private IConfiguration config;
               
        public UserController(IConfiguration config)
        {
            this.config = config;
        }

        /// <summary>
        /// Returns a bool.  If the email exists a true is returned
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        /// <response code="400">If call fails</response>    
        [AllowAnonymous]
        [HttpGet("GetDoesEmailExist/{email}")]
        public IActionResult GetDoesEmailExist(String id)
        {
            try
            {
                var uh = new UserHelper();
                var doesExist = uh.DoesEmailExist(id);
                return Json(doesExist);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("UserController.cs: GetDoesEmailExist(String)", err);
            }
            return Error("UserController.cs: GetDoesEmailExist(String)");
        }

        /// <summary>
        /// Update the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="400">If update fails</response>    
        [SwaggerRequestExample(typeof(User), typeof(UserExample))]
        [HttpPost("Update"), Authorize]
        public IActionResult Update([FromBody] User user)
        {


            return Error("Unexpected error");
        }

        /// <summary>
        /// Returns a JWT token if successful
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        /// <response code="400">If login fails</response>    
        [AllowAnonymous]
        [HttpPost("Create")]
        [SwaggerRequestExample(typeof(Credentials), typeof(CredentialsExample))]
        public IActionResult Create([FromBody] Credentials credentials)
        {
            IActionResult response = Unauthorized();
            var uh = new UserHelper();
            if (!uh.DoesEmailExist(credentials.Email))
            {
                if (credentials != null)
                {
                    var results = uh.CreateUser(credentials.Email, credentials.Password, String.Empty, String.Empty, String.Empty,
                        Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty, String.Empty, Guid.Empty);

                    var tokenString = BuildToken(credentials);
                    response = Ok(new { token = tokenString });

                }
                return response;
            }
            return Error("Unexpected error");
        }

        /// <summary>
        /// Returns a JWT token if the login is successful
        /// </summary>
        /// <param name="credentials">json array containing email and password: {"email": "test@test.com", "password": "TEST1234test!@!"}</param>
        /// <returns></returns>
        /// <response code="201">Returns JWT token</response>
        /// <response code="400">If login fails</response>    
        [AllowAnonymous]
        [HttpPost("Login")]
        [SwaggerRequestExample(typeof(Credentials), typeof(CredentialsExample))]
        public async Task<IActionResult> Login([FromBody] Credentials credentials)
        {
            IActionResult response = Unauthorized();

            //if (ModelState.IsValid)
            {

                if (credentials != null)
                {
                    var tokenString = BuildToken(credentials);
                    response = Ok(new { token = tokenString });
                }
                return response;
            }
            return Error("Unexpected error");
        }

        private string BuildToken(Credentials credentials)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(config["Jwt:Issuer"],
              config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
       

        [HttpPost("Retrieve"), Authorize]
        public IActionResult Retrieve([FromBody] Credentials credentials)
        {
            var uh = new UserHelper();
            var user = uh.LoginUser(credentials.Email, credentials.Password);
            return Json(user);
        }

        private string GetIdToken(IdentityUser user)
        {
            var payload = new Dictionary<string, object>
            {
                { "id", user.Id },
                { "sub", user.Email },
                { "email", user.Email },
                { "emailConfirmed", user.EmailConfirmed },
            };
            return GetToken(payload);
        }

        private string GetAccessToken(string Email)
        {
            var payload = new Dictionary<string, object>
            {
                { "sub", Email },
                { "email", Email }
            };
            return GetToken(payload);
        }

        private string GetToken(Dictionary<string, object> payload)
        {
            var secret = options.SecretKey;

            payload.Add("iss", options.Issuer);
            payload.Add("aud", options.Audience);
            payload.Add("nbf", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("iat", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("exp", ConvertToUnixTimestamp(DateTime.Now.AddDays(7)));
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            return encoder.Encode(payload, secret);
        }

        private static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        /*
        // GET api/User/5
        [HttpGet("{Id}")]
        public string Get(Guid Id)
        {
            return "value";
        }
        */
        /*
        // POST api/User
        [HttpPost]
        public void Post([FromBody]string value)
        {

        }*/
        /*
        [HttpPost("{username}/{password}")]
        // POST api/User/Login
        //[Route("{username:string,password:string}/login")]
        public bool Login(string username, String password)
        {
            return (true);
        }
        */
        /*
        // PUT api/User/5
        [HttpPut("{Id}")]
        public void Put(Guid Id, [FromBody]string value)
        {

        }
        */
        /*
        // DELETE api/User/5
        [HttpDelete("{Id}")]
        public void Delete(Guid id)
        {

        }
        */
    }
}
