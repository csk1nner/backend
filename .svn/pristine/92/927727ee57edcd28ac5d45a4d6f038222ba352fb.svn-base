﻿using DynamicConnections.Nutristyle.Dynamics8Helpers;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json.Linq;
using NutriStyle.WebAPI.RESTfulModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace NutriStyle.WebAPI.RESTfulHelpers
{
    //http://www.inogic.com/blog/2016/02/set-values-of-all-data-types-using-web-api-in-dynamics-crm/
    public class UserHelper
    {

        public User LoginUser(String email, String password)
        {
            try
            {
                var contactFetchXml = @"<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0' >
                    <entity name='contact'> 
                        <attribute name='firstname'/>
                        <attribute name='emailaddress1'/> 
                        <attribute name='lastname'/> 
                        <attribute name='gendercode'/>
                        <attribute name='dc_targetweight'/>
                        <attribute name='dc_currentweight'/>
                        <attribute name='dc_heightfeet'/>
                        <attribute name='dc_heightinches'/>
                        <attribute name='birthdate'/>
                        <attribute name='dc_activitylevel'/>
                        <attribute name='dc_poundsperweek'/>
                        <attribute name='dc_maintaintargetweight'/>
                        <attribute name='dc_kcalcalculatedtarget'/>
                        <attribute name='dc_weightkg'/> 
                        <attribute name='dc_heightcm'/> 
                        <attribute name='dc_age'/>
                        <attribute name='dc_bmi'/>
                            
                        <attribute name='contactid'/>

                        <attribute name='dc_morningsnack'/>
                        <attribute name='dc_afternoonsnack'/>
                        <attribute name='dc_eveningsnack'/>
                        <attribute name='dc_menupresetid'/>
                        <attribute name='dc_dee'/>
                        <attribute name='dc_userspecifiedkcaltarget'/>
                        <attribute name='dc_kcaltarget'/>
                            
                        <attribute name='dc_rollshoppinglisttoparent'/>
                        <filter type='and'>
                            <condition attribute='emailaddress1' value='@EMAIL' operator='eq'/>
                            <condition attribute='dc_password' value='@PASSWORD' operator='eq'/>
                        </filter>
                        <link-entity name='dc_menu' alias='dc_menu' to='contactid' from='dc_contactid' link-type='outer'>
                                <attribute name='dc_menuid'/>
                            <filter type='and'> <condition attribute='dc_primarymenu' value='1' operator='eq'/> 
                            </filter> 
                        </link-entity>
                    </entity> 
                </fetch>";

                contactFetchXml = contactFetchXml.Replace("@EMAIL", email);
                contactFetchXml = contactFetchXml.Replace("@PASSWORD", password);

                //var a = new Authenticate();
                var execute = new DynamicsConnection();
                var results = execute.FetchXML(DynamicsConnection.RegardingTypeCode.Contact, contactFetchXml).Result;
                var cf = new ConvertFrom();
                var user = cf.Execute<User>(DynamicsConnection.RegardingTypeCode.Contact, results);
                return (user);
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("UserHelper.cs: LoginUser(String, String)", err);
            }
            return (null);
        }
        public bool DoesEmailExist(String email)
        {
            try
            {
                var presetId = Guid.Empty;
                var fetchXml = @"<fetch aggregate='true'>
                      <entity name='contact'>
                        <attribute name='emailaddress1' aggregate='count' alias='emailcount'/> 
                        <filter type='and'>
                          <condition attribute='emailaddress1' operator='eq' value='@EMAILADDRESS' />
                        </filter>
                      </entity>
                    </fetch>";

                fetchXml = fetchXml.Replace("@EMAILADDRESS", email);
                var execute = new DynamicsConnection();
                var results = execute.Aggregate(DynamicsConnection.RegardingTypeCode.Contact, fetchXml).Result;
                var cf = new ConvertFrom();
                var count = cf.Aggregate(results, "emailcount");
                if(count > 0)
                {
                    return (true);
                }
            }
            catch (Exception err)
            {
                Globals.LogHelper.CreateLog("UserHelper.cs: DoesEmailExist(String)", err);
            }
            return (false);
        }


        public User CreateUser(String emailAddress, String password, String zipCode, 
            String firstname, String lastname,
            Guid grocerPrimaryId, Guid grocerSecondaryId, Guid grocerTertiaryId, 
            Guid countryId, String grocerOther, Guid verificationCodeId)
        {
            
            try
            {
                /*
                EntityCollection ec = CrmHelper.GetEntitiesByAttribute("contact", "emailaddress1", emailAddress, new String[] { "contactid" }, null, crmService);
                if (ec.Entities.Count > 0)
                {
                    xml = Error.Create("Please use an other email address.  This one is in use");
                }*/

                //else//create user
                {
                    //Relate this contact to the default preset
                    var presetId = Guid.Empty;
                    var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='dc_presets'>
                        <attribute name='dc_presetsid' />
                        <filter type='and'>
                          <condition attribute='dc_defaultpreset' operator='eq' value='1' />
                        </filter>
                      </entity>
                    </fetch>";

                    var execute = new DynamicsConnection();
                    var results = execute.FetchXML(DynamicsConnection.RegardingTypeCode.Preset, fetchXml).Result;
                    var cf = new ConvertFrom();
                    var preset = (Preset)cf.Execute<Preset>(DynamicsConnection.RegardingTypeCode.Preset, results);
                    presetId = preset.PresetId;


                    var entity = new JObject
                    {
                        { "dc_password", password },
                        { "emailaddress1", emailAddress },
                        { "address1_postalcode", zipCode },
                        { "firstname", firstname },
                        { "lastname", lastname },
                        { "dc_grocerother", grocerOther },
                    };

                    
                    if (grocerPrimaryId != Guid.Empty)
                    {
                        entity.Add("dc_grocerprimaryid@odata.bind", "/dc_grocers(" + grocerPrimaryId + ")");
                    }
                    
                    if (grocerSecondaryId != Guid.Empty)
                    {
                        entity.Add("dc_grocersecondaryid@odata.bind", "/dc_grocers(" + grocerSecondaryId + ")");
                    }

                    if (grocerTertiaryId != Guid.Empty)
                    {
                        entity.Add("dc_grocertertiaryid@odata.bind", "/dc_grocers(" + grocerTertiaryId + ")");
                    }

                    
                    if (countryId != Guid.Empty)
                    {
                        entity.Add("dc_countryid@odata.bind", "/dc_countries(" + countryId + ")");
                    }
                    
                    if (verificationCodeId != Guid.Empty)
                    {
                        entity.Add("dc_verifycustomerid@odata.bind", "/dc_verifycustomers(" + verificationCodeId + ")");
                    }
                    

                    var dc = new DynamicsConnection();
                    var Id =  dc.Create(DynamicsConnection.RegardingTypeCode.Contact, entity).Result;


                }
            }
            catch (Exception err)
            {
                /*
                logger.error(e.Message);
                logger.error(e.StackTrace);*/
                Globals.LogHelper.CreateLog("UserHelper.cs: CreateUser()", err);
            }
            return (null);
        }

    }
}
